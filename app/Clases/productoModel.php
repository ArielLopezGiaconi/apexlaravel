<?php

namespace App\Clases;

use Illuminate\Database\Eloquent\Model;
use App\Clases\Centro;
use App\Clases\Impuesto;
use App\Clases\Sociedad;

// require_once 'UMB.php';
// require_once 'Embalaje.php';
// require_once 'SubEmbalaje.php';
// require_once 'Medidas.php';
// require_once 'Sociedad.php';
// require_once 'Centro.php';
// require_once 'Impuesto.php';

class productoModel extends Model
{
    var $codMaterial = 0;
    var $codAnterior = "";
    var $tipoProd = "";
    var $desCorta = "";
    var $desLarga = "";
    var $codJerarquia = "";
    var $codLinea = 0;
    var $deslinea = "";
    var $codSeccion = 0;
    var $desSeccion = "";
    var $codRubro = 0;
    var $desRubro = "";
    var $codSubRubro = 0;
    var $desSubRubro = "";
    var $codGrupo = 0;
    var $desGrupo = "";
    var $codMarca = 0;
    var $desMarca = "";
    var $pesoBruto = 0;
    var $pesoNeto = 0;
    var $uniPeso = 0;
    var $volumen = 0;
    var $uniVolumen = "";
    var $uniBase = "";
    var $uniEmbalaje = "";
    var $denConvEmb = 0;
    var $numConvEmb = 0;
    var $pesoNetoEmbalaje = 0;
    var $pesoBrutoEmbalaje = 0;
    var $uniSubEmbalaje = "";
    var $denConvSubEmb = "";
    var $numConvSubEmb = 0;
    var $pesoNetoSubEmb = 0;
    var $pesoBrutoSubEmb = 0;
    var $UMB;
    var $embalaje; 
    var $subEmbalaje;
    var $VolumenEmb;
    var $uniPesoEmb;
    var $uniVolumenEmb;
    var $volumenSubEmb;
    var $uniPesoSubEmb;
    var $uniVolumenSubEmb;
    var $sociedades = array();
    var $impuestos = array();
    var $medidas;
    var $centros = array();

    public function __construct()
    {
        // parent::__construct();
        // $this->UMB = new UMB();
        // $this->embalaje = new Embalaje();
        // $this->subEmbalaje = new SubEmbalaje();
        // $this->medidas = new Medidas();
    }

    public function insertProducto()
    {
        return $this->insertDimerc();
    }

    public function addSociedad($codMaterial, $organizacionVentas, $canalDist, $uniVenta,
                                $costoMedioVariable, $costoComercial, $desPeru, $aPedido,
                                $grupoMaterial, $marcaPropia, $importado, $afecto, $catComision, $codneg)
    {
        $sociedad = new Sociedad($codMaterial, $organizacionVentas, $canalDist, $uniVenta,
                                $costoMedioVariable, $costoComercial, $desPeru, $aPedido,
                                $grupoMaterial, $marcaPropia, $importado, $afecto, $catComision, $codneg);
        array_push($this->sociedades, $sociedad);
    }

    public function addImpuesto($pais, $impEspecifico, $clasifFiscal)
    {
        $impuesto = new Impuesto($pais, $impEspecifico, $clasifFiscal);
        array_push($this->impuestos, $impuesto);
    }

    public function addCentro($codCentro, $rotacion)
    {
        $centro = new Centro($codCentro, $rotacion);
        array_push($this->centros, $centro);
    }

}