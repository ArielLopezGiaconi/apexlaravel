<?php 
	/**
	 * 
	 */
	namespace App\Clases;
	
	class Sociedad
	{
		public function __construct($codMaterial, $organizacionVentas, $canalDist, $uniVenta,
                                $costoMedioVariable, $costoComercial, $desPeru, $aPedido,
                                $grupoMaterial, $marcaPropia, $importado, $afecto, $catComision, $codneg)
	    {
	        //parent::__construct();
	        $this->codMaterial = $codMaterial;
	        $this->orgVentas = $organizacionVentas;
	        $this->canalDist = $canalDist;
	        $this->uniVenta = $uniVenta;
	        $this->costoMedioVariable = $costoMedioVariable;
	        $this->costoComercial = $costoComercial; 
	        $this->desPeru = $desPeru;
	        $this->aPedido = $aPedido;
	        $this->grupoMaterial = $grupoMaterial;
	        $this->marcaPropia = $marcaPropia;
	        $this->importado = $importado;
	        $this->afecto = $afecto;
	        $this->catComision = $catComision;
	        $this->codneg = $codneg;
	    }

		var $codMaterial = 0;
		var $orgVentas = 0;
		var $canalDist = 0;
		var $costoMedioVariable = "";
		var $costoComercial = 0;
		var $desPeru = "";
		var $aPedido = "";
		var $grupoMaterial = "";
		var $marcaPropia = "";
		var $importado = "";
		var $afecto = "";
		var $catComision = "";
		var $codneg = 0;
	}
 ?>