<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApexPDO extends Model
{

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------- DIMERC -------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static function Dimerc($producto)
    {        
        $pais = "";
        $impEspecifico = "";
        $clasifFiscal = "";
        $tipoprod = "";
        $sociedad = "";

        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas = 2000) {
                $sociedad = $soc;
            }
        }

        $grupoMaterial = $sociedad->grupoMaterial;

        if (empty($sociedad->marcaPropia)) {
            $marca_propia = 'N';
        }else{
            $marca_propia = $sociedad->marcaPropia;
        }
        
        if (empty($sociedad->importado)) {
            $importado = 'N';
        }else{
            $importado = 'S';
        }

        if ($sociedad->afecto = 'Afecto') {
            $afecto = 'S';
        }else{
            $afecto = 'N';
        }

        if ($sociedad->codneg = 'Z00') { 
            $codneg = 0; 
            $tipoprod =  1;
        }else{ 
            if ($sociedad->codneg = 'Z01') { 
                $codneg = 2; 
                $tipoprod = 1; 
            }else{ 
                if ($sociedad->codneg = 'Z02') { 
                    $codneg = 1; 
                    $tipoprod = 1;
                }else{
                    if ($sociedad->codneg = 'Z03') { 
                        $codneg = 0; 
                        $tipoprod = 2; 
                    }
                }
            }
        } 
    
        if ($grupoMaterial = 'Z01') {
            $sobreVenta = 'S'; 
            $pedido = 'S';
        }else{
            if ($grupoMaterial = 'Z02') {
                $sobreVenta = 'N'; 
                $pedido = 'S';
            }else{  
                if ($grupoMaterial = 'Z04') {
                    $sobreVenta = 'S'; 
                    $pedido = 'P';
                }
            }
        }

        $centro = end($producto->centros);

        foreach ($producto->impuestos as $impu)
        {
            if ($impu->impEspecifico <> 'ZMW1' ) {
                if ($impu->clasifFiscal <> 0) {
                    if ($impu->pais = 'CL') {
                        $impEspecifico = $impu->impEspecifico;
                        $clasifFiscal = $impu->clasifFiscal;
                        $pais = $impu->pais;
                    }
                }
            }
        }

        // if ($sociedad->codneg = 'Z01') { //ESTADO PRODUCTO, MAPEADO PARA DIMERC CHILE Y PERÚ
        //     $estpro = 1;
        // }else{
        //     if ($sociedad->codneg = 'Z02') {  
        //         $estpro = 3;
        //     }else{
        //         if ($sociedad->codneg = 'Z03') { 
        //             $estpro = 2;
        //         }else{
        //             if ($sociedad->codneg = 'Z04') { 
        //                 $estpro = 5;
        //             }else{
        //                 if ($sociedad->codneg = 'Z05') { 
        //                     $estpro = 0;
        //                 }else{
        //                     $estpro = 0;
        //                 }
        //             }
        //         }
        //     }
        // }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_producto ------------------------------------------------------------------------------------------------------
        $sql = "select codpro from ma_product where codpro = :codpro";

        if(empty($producto->codAnterior)){
            $codigo = $producto->codMaterial;
        }else{
            $codigo = $producto->codAnterior;
        }

        $resultado = DB::connection('oracle_dmventasQA')->select($sql, [
            'codpro' => $codigo,
        ]);
        

        if(empty($resultado)){
            
            $sql = "INSERT INTO MA_PRODUCT
                            (CODPRO, CODLIN, CODFAM, CODMAR, CODMOD, COSTO, ULTCOS, CODBDG, UNMIVT, MONEDA, COLATE, TIPALM, ESTPRO
                            ,CODIGO_UNI, TIPOPROD_SAP, AFECTO, DESPRO, DESPROCORTA, DESPROLARGA, CODJER, CODLIN_SAP, DESLIN, CODSEC, DESSEC, CODRUB, DESRUB, CODSUBGRU, DESSUBGRU, CODGRU
                            ,DESGRU, CODMAR_SAP, DESMAR, PESO, PESO_NETO, UNI_PESO, VOLUMEN, UNI_VOLUMEN, CODUNI_SAP, CODSAP, UNI_EMBALAJE, DEN_CONV_EMB, EMBALAJE, PESO_NETO_EMB, PESO_EMB
                            ,UNI_SUB_EMB, DEN_CONV_SUB_EMB, SUB_EMBALAJE, PESO_NETO_SUB_EMB, PESO_BRUTO_SUB_EMB, VOLUMEN_EMB, UNI_PESO_EMB, UNI_VOLUMEN_EMB, VOLUMEN_SUB_EMB, UNI_PESO_SUB_EMB
                            ,UNI_VOLUMEN_SUB_EMB, INDSBR, TIPROT, MARCA_PROPIA, IMPORTADO, CATEGORIA, CODNEG, TIPOPROD, PAIS, COD_IMPUESTO, IMPUESTO, PEDSTO, COSPROM, FECING)
                    VALUES(:CODPRO, :CODLIN, :CODFAM, :CODMAR, :CODMOD, :COSTO, :ULTCOS, :CODBDG, :UNMIVT, :MONEDA, :COLATE, :TIPALM, :ESTPRO
                            ,:CODIGO_UNI, :TIPOPROD_SAP, :AFECTO, :DESPRO, :DESPROCORTA, :DESPROLARGA, :CODJER, :CODLIN_SAP, :DESLIN, :CODSEC, :DESSEC, :CODRUB, :DESRUB, :CODSUBGRU, :DESSUBGRU,:CODGRU  
                            ,:DESGRU, :CODMAR_SAP, :DESMAR, :PESO, :PESO_NETO, :UNI_PESO, :VOLUMEN, :UNI_VOLUMEN, :CODUNI_SAP, :CODSAP, :UNI_EMBALAJE, :DEN_CONV_EMB, :EMBALAJE, :PESO_NETO_EMB, :PESO_EMB
                            ,:UNI_SUB_EMB, :DEN_CONV_SUB_EMB, :SUB_EMBALAJE, :PESO_NETO_SUB_EMB, :PESO_BRUTO_SUB_EMB, :VOLUMEN_EMB, :UNI_PESO_EMB, :UNI_VOLUMEN_EMB, :VOLUMEN_SUB_EMB, :UNI_PESO_SUB_EMB
                            ,:UNI_VOLUMEN_SUB_EMB, :INDSBR, :TIPROT, :MARCA_PROPIA, :IMPORTADO, :CATEGORIA, :CODNEG, :TIPOPROD, :PAIS, :COD_IMPUESTO, :IMPUESTO, :PEDSTO, :COSPROM, SYSDATE)";    

                DB::connection('oracle_dmventasQA')->statement($sql,[ 
                    'CODPRO' => $codigo, 'CODLIN' => 20, 'CODFAM' => 1,
                    'CODMAR' => 2976, 'CODMOD' => 0, 'COSTO' => $sociedad->costoMedioVariable,
                    'ULTCOS' => 0, 'CODBDG' => 0, 'UNMIVT' => 0,
                    'MONEDA' => 0, 'COLATE' => 0, 'TIPALM' => 1,
                    'ESTPRO' => 11, 'CODIGO_UNI' => $producto->codAnterior, 'TIPOPROD_SAP' => $producto->tipoProd,
                    'AFECTO' => $afecto, 'DESPRO' => $producto->desLarga, 'DESPROCORTA' => $producto->desCorta,
                    'DESPROLARGA' => $producto->desLarga, 'CODJER' => $producto->codJerarquia, 'CODLIN_SAP' => $producto->codLinea,
                    'DESLIN' => $producto->deslinea, 'CODSEC' => $producto->codSeccion, 'DESSEC' => $producto->desSeccion,
                    'CODRUB' => $producto->codRubro, 'DESRUB' => $producto->desRubro, 'CODSUBGRU' => $producto->codSubRubro,    
                    'DESSUBGRU' => $producto->desSubRubro, 'CODGRU' => $producto->codGrupo, 'DESGRU' => $producto->desGrupo,
                    'CODMAR_SAP' => $producto->codMarca, 'DESMAR' => $producto->desMarca, 'PESO' => $producto->pesoBruto,
                    'PESO_NETO' => $producto->pesoNeto, 'UNI_PESO' => $producto->uniPeso, 'VOLUMEN' => ($producto->volumen / 1000000),
                    'UNI_VOLUMEN' => $producto->uniVolumen, 'CODUNI_SAP' => $producto->uniBase, 'CODSAP' => $producto->codmaterial,
                    'UNI_EMBALAJE' => $producto->uniEmbalaje, 'DEN_CONV_EMB' => $producto->denConvEmb, 'EMBALAJE' => $producto->embalaje,
                    'PESO_NETO_EMB' => $producto->pesoNetoEmbalaje, 'PESO_EMB' => $producto->pesoBrutoEmbalaje, 'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                    'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb, 'SUB_EMBALAJE' => $producto->subEmbalaje, 'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1), 
                    'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100), 'VOLUMEN_EMB' => ($producto->VolumenEmb / 100), 'UNI_PESO_EMB' => $producto->uniPesoEmb,
                    'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb, 'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb, 'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                    'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb, 'INDSBR' => $sobreVenta, 'TIPROT' => $centro->rotacion,
                    'MARCA_PROPIA' => $marca_propia, 'IMPORTADO' => $importado, 'CATEGORIA' => $sociedad->catComision,
                    'CODNEG' => $codneg, 'TIPOPROD' => $tipoprod, 'PAIS' => $pais,
                    'COD_IMPUESTO' => $impEspecifico, 'IMPUESTO' => $clasifFiscal, 'PEDSTO' => $pedido,
                    'COSPROM' => $sociedad->costoMedioVariable]);

            $sql = "INSERT INTO RE_PRODUCTO_SUCURSAL
                    VALUES(3, :codpro, 0)";
    
                DB::connection('oracle_dmventasQA')->statement($sql,[
                    'codpro' => $codigo
                ]);

            $sql = "INSERT INTO RE_PRODUCTO_SUCURSAL
                    VALUES(3, :codpro, 1)";
    
                DB::connection('oracle_dmventasQA')->statement($sql,[
                    'codpro' => $codigo
                ]);

                DB::connection('oracle_dmventasQA')->statement("commit");

        }else{

            $sql = "UPDATE MA_PRODUCT 
                    SET TIPOPROD_SAP = :TIPOPROD
                    , CODMOD = 0
                    , ULTCOS = 0
                    , CODBDG = 0
                    , UNMIVT = 0
                    , MONEDA = 0
                    , COLATE = 0
                    , TIPALM = 1
                    , AFECTO = :AFECTO
                    , DESPRO = :DESPRO
                    , DESPROCORTA = :DESPROCORTA
                    , DESPROLARGA = :DESPROLARGA
                    , CODJER = :CODJER
                    , CODLIN_SAP = :CODLIN
                    , DESLIN = :DESLIN
                    , CODSEC = :CODSEC
                    , DESSEC = :DESSEC
                    , CODRUB = :CODRUB
                    , DESRUB = :DESRUB
                    , CODSUBGRU = :CODSUBGRU
                    , DESSUBGRU = :DESSUBGRU
                    , CODGRU = :CODGRU
                    , DESGRU = :DESGRU
                    , CODMAR_SAP = :CODMAR
                    , DESMAR = :DESMAR
                    , PESO = :PESO
                    , PESO_NETO = :PESO_NETO
                    , UNI_PESO = :UNI_PESO
                    , VOLUMEN = :VOLUMEN
                    , UNI_VOLUMEN = :UNI_VOLUMEN
                    , CODUNI_SAP = :CODUNI
                    , CODSAP = :CODPRO
                    , UNI_EMBALAJE = :UNI_EMBALAJE
                    , DEN_CONV_EMB = :DEN_CONV_EMB
                    , EMBALAJE = nvl(:EMBALAJE,0)
                    , PESO_NETO_EMB = :PESO_NETO_EMB
                    , PESO_EMB = :PESO_EMB
                    , UNI_SUB_EMB = :UNI_SUB_EMB
                    , DEN_CONV_SUB_EMB = :DEN_CONV_SUB_EMB
                    , SUB_EMBALAJE = :SUB_EMBALAJE
                    , PESO_NETO_SUB_EMB = :PESO_NETO_SUB_EMB
                    , PESO_BRUTO_SUB_EMB = :PESO_BRUTO_SUB_EMB
                    , VOLUMEN_EMB = :VOLUMEN_EMB
                    , UNI_PESO_EMB = :UNI_PESO_EMB
                    , UNI_VOLUMEN_EMB = :UNI_VOLUMEN_EMB
                    , VOLUMEN_SUB_EMB = :VOLUMEN_SUB_EMB
                    , UNI_PESO_SUB_EMB = :UNI_PESO_SUB_EMB
                    , UNI_VOLUMEN_SUB_EMB = :UNI_VOLUMEN_SUB_EMB
                    , INDSBR = :INDSBR
                    , TIPROT = :TIPROT
                    , MARCA_PROPIA = :MARCA_PROPIA
                    , IMPORTADO = :IMPORTADO
                    , CATEGORIA = :CATEGORIA
                    , CODNEG = :COD_NEGOCIO
                    , TIPOPROD = :TIP_PROD
                    , PAIS = :PAIS
                    , COD_IMPUESTO = :COD_IMPUESTO
                    , IMPUESTO = :IMPUESTO
                    , PEDSTO = :PEDIDO
                    WHERE CODSAP = :CODPRO1";

            DB::connection('oracle_dmventasQA')->statement($sql,[
                'TIPOPROD' => $producto->tipoProd,
                'AFECTO' => $afecto,
                'DESPRO' => $producto->desLarga,
                'DESPROCORTA' => $producto->desCorta,
                'DESPROLARGA' => $producto->desLarga,
                'CODJER' => $producto->codJerarquia,
                'CODLIN' => $producto->codLinea,
                'DESLIN' => $producto->deslinea,
                'CODSEC' => $producto->codSeccion,
                'DESSEC' => $producto->desSeccion,
                'CODRUB' => $producto->codRubro,
                'DESRUB' => $producto->desRubro,
                'CODSUBGRU' => $producto->codSubRubro,
                'DESSUBGRU' => $producto->desSubRubro,
                'CODGRU' => $producto->codGrupo,
                'DESGRU' => $producto->desGrupo,
                'CODMAR' => $producto->codMarca,
                'DESMAR' => $producto->desMarca,
                'PESO' => $producto->pesoBruto,
                'PESO_NETO' => $producto->pesoNeto,
                'UNI_PESO' => $producto->uniPeso,
                'VOLUMEN' => ($producto->volumen / 1000000),
                'UNI_VOLUMEN' => $producto->uniVolumen,
                'CODUNI' => $producto->uniBase,
                'CODPRO' => $producto->codMaterial,
                'UNI_EMBALAJE' => $producto->uniEmbalaje,
                'DEN_CONV_EMB' => $producto->denConvEmb,
                'EMBALAJE' => $producto->embalaje,
                'PESO_NETO_EMB' => ($producto->pesoNetoEmbalaje * 1),
                'PESO_EMB' => ($producto->pesoBrutoEmbalaje * 1),
                'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb,
                'SUB_EMBALAJE' => $producto->subEmbalaje,
                'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1),
                'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100),
                'VOLUMEN_EMB' => ($producto->VolumenEmb / 100),
                'UNI_PESO_EMB' => $producto->uniPesoEmb,
                'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb,
                'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb,
                'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb,
                'INDSBR' => $sobreVenta,
                'TIPROT' => $centro->rotacion,
                'MARCA_PROPIA' => $marca_propia,
                'IMPORTADO' => $importado,
                'CATEGORIA' => $sociedad->catComision,
                'COD_NEGOCIO' => $codneg,
                'TIP_PROD' => $tipoprod,
                'PAIS' => $pais,
                'COD_IMPUESTO' => $impEspecifico,
                'IMPUESTO' => $clasifFiscal,
                'PEDIDO' => $pedido,
                'CODPRO1' => $producto->codMaterial
            ]);

            DB::connection('oracle_dmventasQA')->statement("commit");

        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_costosxcentros ------------------------------------------------------------------------------------------------------
        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas == 2000) {
                foreach ($producto->centros as $centroSap)
                {
                    $sql = "select codpro from ma_costosxcentros
                            where centro = :centro
                            and codpro = :codpro
                            and canal = :canal";

                    $resultado = DB::connection('oracle_dmventasQA')->select($sql, [
                        'codpro' => $codigo,
                        'centro' => $centroSap->codCentro,
                        'canal' => $soc->canalDist
                    ]);
            
                    if(empty($resultado)){
                        $sql = "insert into ma_costosxcentros (codemp, centro, codpro, cosprom, canal, fecing) 
                                values(3, :centro, :codpro, :cosprom, :canal, sysdate)";
            
                            DB::connection('oracle_dmventasQA')->statement($sql,[
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'cosprom' => $soc->costoMedioVariable,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_dmventasQA')->statement("commit");

                    }else{
                        $sql = "update ma_costosxcentros set cosprom = :cosprom
                                where centro = :centro
                                and codpro = :codpro
                                and canal = :canal";
            
                            DB::connection('oracle_dmventasQA')->statement($sql,[
                                'cosprom' => $soc->costoMedioVariable,
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_dmventasQA')->statement("commit");
                    }    
                }
            }
        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la re_codebar ----------------------------------------------------------------------------------------------------------------
        if ($producto->UMB = "") {
            $sql = "select codpro from re_codebar where codpro = :codpro and codemp = 3";

            $resultado = DB::connection('oracle_dmventasQA')->select($sql, [
                'codpro' => $codigo,
            ]);

            if(empty($resultado)){
                $sql = "insert into re_codebar (CODPRO,CODBAR,CODEST,FECHA) 
                        values(:codpro, :codbar, 0, sysdate)";
    
                    DB::connection('oracle_dmventasQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_dmventasQA')->statement("commit");
            }else{
                $sql = "update re_codebar set codbar = :codbar 
                        where codpro = :codpro";
    
                    DB::connection('oracle_dmventasQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_dmventasQA')->statement("commit");
            } 
        }

        //  --------------------------------------------------------------------------------------------------------- Inserccion en la re_canprod ----------------------------------------------------------------------------------------------------------------
        $sql = "select codpro from re_canprod where codpro = :codpro and codemp = 3";

        $resultado = DB::connection('oracle_dmventasQA')->select($sql, [
            'codpro' => $codigo,
        ]);

        if(empty($resultado)){
            $sql = "BEGIN DM_VENTAS.SAP_PUT_CREAPRECIOS(3, :codpro); END; ";

                DB::connection('oracle_dmventasQA')->statement($sql,[
                    'codpro' => $codigo,
                ]);

                DB::connection('oracle_dmventasQA')->statement("commit");
        }

        return 'ok';
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------- PERU -------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static function DimercPeru($producto)
    {
        $pais = "";
        $impEspecifico = "";
        $clasifFiscal = "";
        $tipoprod = "";
        $sociedad = "";

        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas = 2000) {
                $sociedad = $soc;
            }
        }

        $grupoMaterial = $sociedad->grupoMaterial;

        if (empty($sociedad->marcaPropia)) {
            $marca_propia = 'N';
        }else{
            $marca_propia = $sociedad->marcaPropia;
        }
        
        if (empty($sociedad->importado)) {
            $importado = 'N';
        }else{
            $importado = 'S';
        }

        if ($sociedad->afecto = 'Afecto') {
            $afecto = 'S';
        }else{
            $afecto = 'N';
        }

        if ($sociedad->codneg = 'Z00') { 
            $codneg = 0; 
            $tipoprod =  1;
        }else{ 
            if ($sociedad->codneg = 'Z01') { 
                $codneg = 2; 
                $tipoprod = 1; 
            }else{ 
                if ($sociedad->codneg = 'Z02') { 
                    $codneg = 1; 
                    $tipoprod = 1;
                }else{
                    if ($sociedad->codneg = 'Z03') { 
                        $codneg = 0; 
                        $tipoprod = 2; 
                    }
                }
            }
        } 
    
        if ($grupoMaterial = 'Z01') {
            $sobreVenta = 'S'; 
            $pedido = 'S';
        }else{
            if ($grupoMaterial = 'Z02') {
                $sobreVenta = 'N'; 
                $pedido = 'S';
            }else{  
                if ($grupoMaterial = 'Z04') {
                    $sobreVenta = 'S'; 
                    $pedido = 'P';
                }
            }
        }

        // if ($sociedad->codneg = 'Z01') { //ESTADO PRODUCTO, MAPEADO PARA DIMERC CHILE Y PERÚ
        //     $estpro = 1;
        // }else{
        //     if ($sociedad->codneg = 'Z02') {  
        //         $estpro = 3;
        //     }else{
        //         if ($sociedad->codneg = 'Z03') { 
        //             $estpro = 2;
        //         }else{
        //             if ($sociedad->codneg = 'Z04') { 
        //                 $estpro = 5;
        //             }else{
        //                 if ($sociedad->codneg = 'Z05') { 
        //                     $estpro = 0;
        //                 }else{
        //                     $estpro = 0;
        //                 }
        //             }
        //         }
        //     }
        // }

        $centro = end($producto->centros);

        foreach ($producto->impuestos as $impu)
        {
            if ($impu->impEspecifico <> 'ZMW1' ) {
                if ($impu->clasifFiscal <> 0) {
                    if ($impu->pais = 'CL') {
                        $impEspecifico = $impu->impEspecifico;
                        $clasifFiscal = $impu->clasifFiscal;
                        $pais = $impu->pais;
                    }
                }
            }
        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_producto ------------------------------------------------------------------------------------------------------
        $sql = "select codpro from ma_product where codpro = :codpro";

        if(empty($producto->codAnterior)){
            $codigo = $producto->codMaterial;
        }else{
            $codigo = $producto->codAnterior;
        }

        $resultado = DB::connection('oracle_dmventasPEQA')->select($sql, [
            'codpro' => $codigo,
        ]);


        if(empty($resultado)){
            
            $sql = "INSERT INTO MA_PRODUCT
                            (CODPRO, CODLIN, CODFAM, CODMAR, CODMOD, COSTO, ULTCOS, CODBDG, UNMIVT, MONEDA, COLATE, TIPALM, ESTPRO
                            ,CODIGO_UNI, TIPOPROD_SAP, AFECTO, DESPRO, DESPROCORTA, DESPROLARGA, CODJER, CODLIN_SAP, DESLIN, CODSEC, DESSEC, CODRUB, DESRUB, CODSUBGRU, DESSUBGRU, CODGRU
                            ,DESGRU, CODMAR_SAP, DESMAR, PESO, PESO_NETO, UNI_PESO, VOLUMEN, UNI_VOLUMEN, CODUNI_SAP, CODSAP, UNI_EMBALAJE, DEN_CONV_EMB, EMBALAJE, PESO_NETO_EMB, PESO_EMB
                            ,UNI_SUB_EMB, DEN_CONV_SUB_EMB, SUB_EMBALAJE, PESO_NETO_SUB_EMB, PESO_BRUTO_SUB_EMB, VOLUMEN_EMB, UNI_PESO_EMB, UNI_VOLUMEN_EMB, VOLUMEN_SUB_EMB, UNI_PESO_SUB_EMB
                            ,UNI_VOLUMEN_SUB_EMB, INDSBR, TIPROT, MARCA_PROPIA, IMPORTADO, CATEGORIA, CODNEG, TIPOPROD, PAIS, COD_IMPUESTO, IMPUESTO, PEDSTO, COSPROM, FECING)
                    VALUES(:CODPRO, :CODLIN, :CODFAM, :CODMAR, :CODMOD, :COSTO, :ULTCOS, :CODBDG, :UNMIVT, :MONEDA, :COLATE, :TIPALM, :ESTPRO
                            ,:CODIGO_UNI, :TIPOPROD_SAP, :AFECTO, :DESPRO, :DESPROCORTA, :DESPROLARGA, :CODJER, :CODLIN_SAP, :DESLIN, :CODSEC, :DESSEC, :CODRUB, :DESRUB, :CODSUBGRU, :DESSUBGRU,:CODGRU  
                            ,:DESGRU, :CODMAR_SAP, :DESMAR, :PESO, :PESO_NETO, :UNI_PESO, :VOLUMEN, :UNI_VOLUMEN, :CODUNI_SAP, :CODSAP, :UNI_EMBALAJE, :DEN_CONV_EMB, :EMBALAJE, :PESO_NETO_EMB, :PESO_EMB
                            ,:UNI_SUB_EMB, :DEN_CONV_SUB_EMB, :SUB_EMBALAJE, :PESO_NETO_SUB_EMB, :PESO_BRUTO_SUB_EMB, :VOLUMEN_EMB, :UNI_PESO_EMB, :UNI_VOLUMEN_EMB, :VOLUMEN_SUB_EMB, :UNI_PESO_SUB_EMB
                            ,:UNI_VOLUMEN_SUB_EMB, :INDSBR, :TIPROT, :MARCA_PROPIA, :IMPORTADO, :CATEGORIA, :CODNEG, :TIPOPROD, :PAIS, :COD_IMPUESTO, :IMPUESTO, :PEDSTO, :COSPROM, SYSDATE)";    

                DB::connection('oracle_dmventasPEQA')->statement($sql,[ 
                    'CODPRO' => $codigo, 'CODLIN' => 20, 'CODFAM' => 1,
                    'CODMAR' => 2976, 'CODMOD' => 0, 'COSTO' => $sociedad->costoMedioVariable,
                    'ULTCOS' => 0, 'CODBDG' => 0, 'UNMIVT' => 0,
                    'MONEDA' => 0, 'COLATE' => 0, 'TIPALM' => 1,
                    'ESTPRO' => 11, 'CODIGO_UNI' => $producto->codAnterior, 'TIPOPROD_SAP' => $producto->tipoProd,
                    'AFECTO' => $afecto, 'DESPRO' => $producto->desLarga, 'DESPROCORTA' => $producto->desCorta,
                    'DESPROLARGA' => $producto->desLarga, 'CODJER' => $producto->codJerarquia, 'CODLIN_SAP' => $producto->codLinea,
                    'DESLIN' => $producto->deslinea, 'CODSEC' => $producto->codSeccion, 'DESSEC' => $producto->desSeccion,
                    'CODRUB' => $producto->codRubro, 'DESRUB' => $producto->desRubro, 'CODSUBGRU' => $producto->codSubRubro,    
                    'DESSUBGRU' => $producto->desSubRubro, 'CODGRU' => $producto->codGrupo, 'DESGRU' => $producto->desGrupo,
                    'CODMAR_SAP' => $producto->codMarca, 'DESMAR' => $producto->desMarca, 'PESO' => $producto->pesoBruto,
                    'PESO_NETO' => $producto->pesoNeto, 'UNI_PESO' => $producto->uniPeso, 'VOLUMEN' => ($producto->volumen / 1000000),
                    'UNI_VOLUMEN' => $producto->uniVolumen, 'CODUNI_SAP' => $producto->uniBase, 'CODSAP' => $producto->codMaterial,
                    'UNI_EMBALAJE' => $producto->uniEmbalaje, 'DEN_CONV_EMB' => $producto->denConvEmb, 'EMBALAJE' => $producto->embalaje,
                    'PESO_NETO_EMB' => ($producto->pesoNetoEmbalaje * 1), 'PESO_EMB' => ($producto->pesoBrutoEmbalaje * 1), 'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                    'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb, 'SUB_EMBALAJE' => $producto->subEmbalaje, 'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1), 
                    'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100), 'VOLUMEN_EMB' => ($producto->VolumenEmb / 100), 'UNI_PESO_EMB' => $producto->uniPesoEmb,
                    'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb, 'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb, 'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                    'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb, 'INDSBR' => $sobreVenta, 'TIPROT' => $centro->rotacion,
                    'MARCA_PROPIA' => $marca_propia, 'IMPORTADO' => $importado, 'CATEGORIA' => $sociedad->catComision,
                    'CODNEG' => $codneg, 'TIPOPROD' => $tipoprod, 'PAIS' => $pais,
                    'COD_IMPUESTO' => $impEspecifico, 'IMPUESTO' => $clasifFiscal, 'PEDSTO' => $pedido,
                    'COSPROM' => $sociedad->costoMedioVariable]);

                DB::connection('oracle_dmventasPEQA')->statement("commit");

        }else{

            $sql = "UPDATE MA_PRODUCT 
                    SET TIPOPROD_SAP = :TIPOPROD
                    , CODMOD = 0
                    , ULTCOS = 0
                    , CODBDG = 0
                    , UNMIVT = 0
                    , MONEDA = 0
                    , COLATE = 0
                    , TIPALM = 1
                    , AFECTO = :AFECTO
                    , DESPRO = :DESPRO
                    , DESPROCORTA = :DESPROCORTA
                    , DESPROLARGA = :DESPROLARGA
                    , CODJER = :CODJER
                    , CODLIN_SAP = :CODLIN
                    , DESLIN = :DESLIN
                    , CODSEC = :CODSEC
                    , DESSEC = :DESSEC
                    , CODRUB = :CODRUB
                    , DESRUB = :DESRUB
                    , CODSUBGRU = :CODSUBGRU
                    , DESSUBGRU = :DESSUBGRU
                    , CODGRU = :CODGRU
                    , DESGRU = :DESGRU
                    , CODMAR_SAP = :CODMAR
                    , DESMAR = :DESMAR
                    , PESO = :PESO
                    , PESO_NETO = :PESO_NETO
                    , UNI_PESO = :UNI_PESO
                    , VOLUMEN = :VOLUMEN
                    , UNI_VOLUMEN = :UNI_VOLUMEN
                    , CODUNI_SAP = :CODUNI
                    , CODSAP = :CODPRO
                    , UNI_EMBALAJE = :UNI_EMBALAJE
                    , DEN_CONV_EMB = :DEN_CONV_EMB
                    , EMBALAJE = nvl(:EMBALAJE,0)
                    , PESO_NETO_EMB = :PESO_NETO_EMB
                    , PESO_EMB = :PESO_EMB
                    , UNI_SUB_EMB = :UNI_SUB_EMB
                    , DEN_CONV_SUB_EMB = :DEN_CONV_SUB_EMB
                    , SUB_EMBALAJE = :SUB_EMBALAJE
                    , PESO_NETO_SUB_EMB = :PESO_NETO_SUB_EMB
                    , PESO_BRUTO_SUB_EMB = :PESO_BRUTO_SUB_EMB
                    , VOLUMEN_EMB = :VOLUMEN_EMB
                    , UNI_PESO_EMB = :UNI_PESO_EMB
                    , UNI_VOLUMEN_EMB = :UNI_VOLUMEN_EMB
                    , VOLUMEN_SUB_EMB = :VOLUMEN_SUB_EMB
                    , UNI_PESO_SUB_EMB = :UNI_PESO_SUB_EMB
                    , UNI_VOLUMEN_SUB_EMB = :UNI_VOLUMEN_SUB_EMB
                    , INDSBR = :INDSBR
                    , TIPROT = :TIPROT
                    , MARCA_PROPIA = :MARCA_PROPIA
                    , IMPORTADO = :IMPORTADO
                    , CATEGORIA = :CATEGORIA
                    , CODNEG = :COD_NEGOCIO
                    , TIPOPROD = :TIP_PROD
                    , PAIS = :PAIS
                    , COD_IMPUESTO = :COD_IMPUESTO
                    , IMPUESTO = :IMPUESTO
                    , PEDSTO = :PEDIDO
                    WHERE CODSAP = :CODPRO1";

            DB::connection('oracle_dmventasPEQA')->statement($sql,[
                'TIPOPROD' => $producto->tipoProd,
                'AFECTO' => $afecto,
                'DESPRO' => $producto->desLarga,
                'DESPROCORTA' => $producto->desCorta,
                'DESPROLARGA' => $producto->desLarga,
                'CODJER' => $producto->codJerarquia,
                'CODLIN' => $producto->codLinea,
                'DESLIN' => $producto->deslinea,
                'CODSEC' => $producto->codSeccion,
                'DESSEC' => $producto->desSeccion,
                'CODRUB' => $producto->codRubro,
                'DESRUB' => $producto->desRubro,
                'CODSUBGRU' => $producto->codSubRubro,
                'DESSUBGRU' => $producto->desSubRubro,
                'CODGRU' => $producto->codGrupo,
                'DESGRU' => $producto->desGrupo,
                'CODMAR' => $producto->codMarca,
                'DESMAR' => $producto->desMarca,
                'PESO' => $producto->pesoBruto,
                'PESO_NETO' => $producto->pesoNeto,
                'UNI_PESO' => $producto->uniPeso,
                'VOLUMEN' => ($producto->volumen / 1000000),
                'UNI_VOLUMEN' => $producto->uniVolumen,
                'CODUNI' => $producto->uniBase,
                'CODPRO' => $producto->codMaterial,
                'UNI_EMBALAJE' => $producto->uniEmbalaje,
                'DEN_CONV_EMB' => $producto->denConvEmb,
                'EMBALAJE' => $producto->embalaje,
                'PESO_NETO_EMB' => ($producto->pesoNetoEmbalaje * 1),
                'PESO_EMB' => ($producto->pesoBrutoEmbalaje * 1),
                'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb,
                'SUB_EMBALAJE' => $producto->subEmbalaje,
                'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1),
                'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100),
                'VOLUMEN_EMB' => ($producto->VolumenEmb / 100),
                'UNI_PESO_EMB' => $producto->uniPesoEmb,
                'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb,
                'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb,
                'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb,
                'INDSBR' => $sobreVenta,
                'TIPROT' => $centro->rotacion,
                'MARCA_PROPIA' => $marca_propia,
                'IMPORTADO' => $importado,
                'CATEGORIA' => $sociedad->catComision,
                'COD_NEGOCIO' => $codneg,
                'TIP_PROD' => $tipoprod,
                'PAIS' => $pais,
                'COD_IMPUESTO' => $impEspecifico,
                'IMPUESTO' => $clasifFiscal,
                'PEDIDO' => $pedido,
                'CODPRO1' => $producto->codMaterial
            ]);

            DB::connection('oracle_dmventasPEQA')->statement("commit");

        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_costosxcentros ------------------------------------------------------------------------------------------------------
        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas == 2000) {
                foreach ($producto->centros as $centroSap)
                {
                    $sql = "select codpro from ma_costosxcentros
                            where centro = :centro
                            and codpro = :codpro
                            and canal = :canal";

                    $resultado = DB::connection('oracle_dmventasPEQA')->select($sql, [
                        'codpro' => $codigo,
                        'centro' => $centroSap->codCentro,
                        'canal' => $soc->canalDist
                    ]);
            
                    if(empty($resultado)){
                        $sql = "insert into ma_costosxcentros (codemp, centro, codpro, cosprom, canal, fecha) 
                                values(3, :centro, :codpro, :cosprom, :canal, sysdate)";
            
                            DB::connection('oracle_dmventasPEQA')->statement($sql,[
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'cosprom' => $soc->costoMedioVariable,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_dmventasPEQA')->statement("commit");

                    }else{
                        $sql = "update ma_costosxcentros set cosprom = :cosprom
                                where centro = :centro
                                and codpro = :codpro
                                and canal = :canal";
            
                            DB::connection('oracle_dmventasPEQA')->statement($sql,[
                                'cosprom' => $soc->costoMedioVariable,
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_dmventasPEQA')->statement("commit");

                    }    
                }
            }
        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la re_codebar ----------------------------------------------------------------------------------------------------------------
        if ($producto->UMB = "") {
            $sql = "select codpro from re_codebar where codpro = :codpro and codemp = 3";

            $resultado = DB::connection('oracle_dmventasPEQA')->select($sql, [
                'codpro' => $codigo,
            ]);

            if(empty($resultado)){
                $sql = "insert into re_codebar (CODPRO,CODBAR,CODEST,FECHA) 
                        values(:codpro, :codbar, 0, sysdate)";
    
                    DB::connection('oracle_dmventasPEQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_dmventasPEQA')->statement("commit");
            }else{
                $sql = "update re_codebar set codbar = :codbar 
                        where codpro = :codpro";
    
                    DB::connection('oracle_dmventasPEQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_dmventasPEQA')->statement("commit");
            } 
        }

        //  --------------------------------------------------------------------------------------------------------- Inserccion en la re_canprod ----------------------------------------------------------------------------------------------------------------
        // $sql = "select codpro from re_canprod where codpro = :codpro";

        // $resultado = DB::connection('oracle_dmventasPEQA')->select($sql, [
        //     'codpro' => $codigo,
        // ]);

        // if(empty($resultado)){
        //     $sql = "BEGIN DM_VENTAS.SAP_PUT_CREAPRECIOS(:codpro); END; ";

        //         DB::connection('oracle_dmventasPEQA')->statement($sql,[
        //             'codpro' => $codigo,
        //         ]);

        //         DB::connection('oracle_dmventasPEQA')->statement("commit");
        // }

        return 'ok';
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------- UNIFICADO -------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static function Unificado($producto)
    {
        $pais = "";
        $impEspecifico = "";
        $clasifFiscal = "";
        $tipoprod = "";
        $sociedad = "";

        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas = 2000) {
                $sociedad = $soc;
            }
        }

        $grupoMaterial = $sociedad->grupoMaterial;

        if (empty($sociedad->marcaPropia)) {
            $marca_propia = 'N';
        }else{
            $marca_propia = $sociedad->marcaPropia;
        }
        
        if (empty($sociedad->importado)) {
            $importado = 'N';
        }else{
            $importado = 'S';
        }

        if ($sociedad->afecto = 'Afecto') {
            $afecto = 'S';
        }else{
            $afecto = 'N';
        }

        if ($sociedad->codneg = 'Z00') { 
            $codneg = 0; 
            $tipoprod =  1;
        }else{ 
            if ($sociedad->codneg = 'Z01') { 
                $codneg = 2; 
                $tipoprod = 1; 
            }else{ 
                if ($sociedad->codneg = 'Z02') { 
                    $codneg = 1; 
                    $tipoprod = 1;
                }else{
                    if ($sociedad->codneg = 'Z03') { 
                        $codneg = 0; 
                        $tipoprod = 2; 
                    }
                }
            }
        } 
    
        if ($grupoMaterial = 'Z01') {
            $sobreVenta = 'S'; 
            $pedido = 'S';
        }else{
            if ($grupoMaterial = 'Z02') {
                $sobreVenta = 'N'; 
                $pedido = 'S';
            }else{  
                if ($grupoMaterial = 'Z04') {
                    $sobreVenta = 'S'; 
                    $pedido = 'P';
                }
            }
        }

        // if ($sociedad->codneg = 'Z01') { //ESTADO PRODUCTO, MAPEADO PARA DIMERC CHILE Y PERÚ
        //     $estpro = 1;
        // }else{
        //     if ($sociedad->codneg = 'Z02') {  
        //         $estpro = 3;
        //     }else{
        //         if ($sociedad->codneg = 'Z03') { 
        //             $estpro = 2;
        //         }else{
        //             if ($sociedad->codneg = 'Z04') { 
        //                 $estpro = 5;
        //             }else{
        //                 if ($sociedad->codneg = 'Z05') { 
        //                     $estpro = 0;
        //                 }else{
        //                     $estpro = 0;
        //                 }
        //             }
        //         }
        //     }
        // }

        $centro = end($producto->centros);

        foreach ($producto->impuestos as $impu)
        {
            if ($impu->impEspecifico <> 'ZMW1' ) {
                if ($impu->clasifFiscal <> 0) {
                    if ($impu->pais = 'CL') {
                        $impEspecifico = $impu->impEspecifico;
                        $clasifFiscal = $impu->clasifFiscal;
                        $pais = $impu->pais;
                    }
                }
            }
        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_producto ------------------------------------------------------------------------------------------------------
        $sql = "select codpro from ma_product where codpro = :codpro";

        if(empty($producto->codAnterior)){
            $codigo = $producto->codMaterial;
        }else{
            $codigo = $producto->codAnterior;
        }

        $resultado = DB::connection('oracle_UnificadoQA')->select($sql, [
            'codpro' => $codigo,
        ]);


        if(empty($resultado)){
            
            $sql = "INSERT INTO MA_PRODUCT
                            (CODPRO, CODLIN, CODMAR
                            ,CODIGO_UNI, TIPOPROD_SAP, AFECTO, DESPRO, DESPROCORTA, DESPROLARGA, CODJER, CODLIN_SAP, DESLIN, CODSEC, DESSEC, CODRUB, DESRUB, CODSUBGRU, DESSUBGRU, CODGRU
                            ,DESGRU, CODMAR_SAP, DESMAR, PESO, PESO_NETO, UNI_PESO, VOLUMEN, UNI_VOLUMEN, CODUNI_SAP, CODSAP, UNI_EMBALAJE, DEN_CONV_EMB, EMBALAJE, PESO_NETO_EMB, PESO_EMB
                            ,UNI_SUB_EMB, DEN_CONV_SUB_EMB, SUB_EMBALAJE, PESO_NETO_SUB_EMB, PESO_BRUTO_SUB_EMB, VOLUMEN_EMB, UNI_PESO_EMB, UNI_VOLUMEN_EMB, VOLUMEN_SUB_EMB, UNI_PESO_SUB_EMB
                            ,UNI_VOLUMEN_SUB_EMB, INDSBR, TIPROT, MARCA_PROPIA, IMPORTADO, CATEGORIA, CODNEG, TIPOPROD, PAIS, COD_IMPUESTO, IMPUESTO, COSPROM, FECING)
                    VALUES(:CODPRO, :CODLIN, :CODMAR
                            ,:CODIGO_UNI, :TIPOPROD_SAP, :AFECTO, :DESPRO, :DESPROCORTA, :DESPROLARGA, :CODJER, :CODLIN_SAP, :DESLIN, :CODSEC, :DESSEC, :CODRUB, :DESRUB, :CODSUBGRU, :DESSUBGRU,:CODGRU  
                            ,:DESGRU, :CODMAR_SAP, :DESMAR, :PESO, :PESO_NETO, :UNI_PESO, :VOLUMEN, :UNI_VOLUMEN, :CODUNI_SAP, :CODSAP, :UNI_EMBALAJE, :DEN_CONV_EMB, :EMBALAJE, :PESO_NETO_EMB, :PESO_EMB
                            ,:UNI_SUB_EMB, :DEN_CONV_SUB_EMB, :SUB_EMBALAJE, :PESO_NETO_SUB_EMB, :PESO_BRUTO_SUB_EMB, :VOLUMEN_EMB, :UNI_PESO_EMB, :UNI_VOLUMEN_EMB, :VOLUMEN_SUB_EMB, :UNI_PESO_SUB_EMB
                            ,:UNI_VOLUMEN_SUB_EMB, :INDSBR, :TIPROT, :MARCA_PROPIA, :IMPORTADO, :CATEGORIA, :CODNEG, :TIPOPROD, :PAIS, :COD_IMPUESTO, :IMPUESTO, :COSPROM, SYSDATE)";    

                DB::connection('oracle_UnificadoQA')->statement($sql,[ 
                    'CODPRO' => $codigo, 'CODLIN' => 20,
                    'CODMAR' => 2976,
                    'CODIGO_UNI' => $producto->codAnterior, 'TIPOPROD_SAP' => $producto->tipoProd,
                    'AFECTO' => $afecto, 'DESPRO' => $producto->desLarga, 'DESPROCORTA' => $producto->desCorta,
                    'DESPROLARGA' => $producto->desLarga, 'CODJER' => $producto->codJerarquia, 'CODLIN_SAP' => $producto->codLinea,
                    'DESLIN' => $producto->deslinea, 'CODSEC' => $producto->codSeccion, 'DESSEC' => $producto->desSeccion,
                    'CODRUB' => $producto->codRubro, 'DESRUB' => $producto->desRubro, 'CODSUBGRU' => $producto->codSubRubro,    
                    'DESSUBGRU' => $producto->desSubRubro, 'CODGRU' => $producto->codGrupo, 'DESGRU' => $producto->desGrupo,
                    'CODMAR_SAP' => $producto->codMarca, 'DESMAR' => $producto->desMarca, 'PESO' => $producto->pesoBruto,
                    'PESO_NETO' => $producto->pesoNeto, 'UNI_PESO' => $producto->uniPeso, 'VOLUMEN' => ($producto->volumen / 1000000),
                    'UNI_VOLUMEN' => $producto->uniVolumen, 'CODUNI_SAP' => $producto->uniBase, 'CODSAP' => $producto->codMaterial,
                    'UNI_EMBALAJE' => $producto->uniEmbalaje, 'DEN_CONV_EMB' => $producto->denConvEmb, 'EMBALAJE' => $producto->embalaje,
                    'PESO_NETO_EMB' => ($producto->pesoNetoEmbalaje * 1), 'PESO_EMB' => ($producto->pesoBrutoEmbalaje * 1), 'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                    'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb, 'SUB_EMBALAJE' => $producto->subEmbalaje, 'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1), 
                    'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100), 'VOLUMEN_EMB' => ($producto->VolumenEmb / 100), 'UNI_PESO_EMB' => $producto->uniPesoEmb,
                    'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb, 'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb, 'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                    'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb, 'INDSBR' => $sobreVenta, 'TIPROT' => $centro->rotacion,
                    'MARCA_PROPIA' => $marca_propia, 'IMPORTADO' => $importado, 'CATEGORIA' => $sociedad->catComision,
                    'CODNEG' => $codneg, 'TIPOPROD' => $tipoprod, 'PAIS' => $pais,
                    'COD_IMPUESTO' => $impEspecifico, 'IMPUESTO' => $clasifFiscal,
                    'COSPROM' => $sociedad->costoMedioVariable]);

                DB::connection('oracle_UnificadoQA')->statement("commit");

        }else{

            $sql = "UPDATE MA_PRODUCT 
                    SET TIPOPROD_SAP = :TIPOPROD
                    , AFECTO = :AFECTO
                    , DESPRO = :DESPRO
                    , DESPROCORTA = :DESPROCORTA
                    , DESPROLARGA = :DESPROLARGA
                    , CODJER = :CODJER
                    , CODLIN_SAP = :CODLIN
                    , DESLIN = :DESLIN
                    , CODSEC = :CODSEC
                    , DESSEC = :DESSEC
                    , CODRUB = :CODRUB
                    , DESRUB = :DESRUB
                    , CODSUBGRU = :CODSUBGRU
                    , DESSUBGRU = :DESSUBGRU
                    , CODGRU = :CODGRU
                    , DESGRU = :DESGRU
                    , CODMAR_SAP = :CODMAR
                    , DESMAR = :DESMAR
                    , PESO = :PESO
                    , PESO_NETO = :PESO_NETO
                    , UNI_PESO = :UNI_PESO
                    , VOLUMEN = :VOLUMEN
                    , UNI_VOLUMEN = :UNI_VOLUMEN
                    , CODUNI_SAP = :CODUNI
                    , CODSAP = :CODPRO
                    , UNI_EMBALAJE = :UNI_EMBALAJE
                    , DEN_CONV_EMB = :DEN_CONV_EMB
                    , EMBALAJE = nvl(:EMBALAJE,0)
                    , PESO_NETO_EMB = :PESO_NETO_EMB
                    , PESO_EMB = :PESO_EMB
                    , UNI_SUB_EMB = :UNI_SUB_EMB
                    , DEN_CONV_SUB_EMB = :DEN_CONV_SUB_EMB
                    , SUB_EMBALAJE = :SUB_EMBALAJE
                    , PESO_NETO_SUB_EMB = :PESO_NETO_SUB_EMB
                    , PESO_BRUTO_SUB_EMB = :PESO_BRUTO_SUB_EMB
                    , VOLUMEN_EMB = :VOLUMEN_EMB
                    , UNI_PESO_EMB = :UNI_PESO_EMB
                    , UNI_VOLUMEN_EMB = :UNI_VOLUMEN_EMB
                    , VOLUMEN_SUB_EMB = :VOLUMEN_SUB_EMB
                    , UNI_PESO_SUB_EMB = :UNI_PESO_SUB_EMB
                    , UNI_VOLUMEN_SUB_EMB = :UNI_VOLUMEN_SUB_EMB
                    , INDSBR = :INDSBR
                    , TIPROT = :TIPROT
                    , MARCA_PROPIA = :MARCA_PROPIA
                    , IMPORTADO = :IMPORTADO
                    , CATEGORIA = :CATEGORIA
                    , CODNEG = :COD_NEGOCIO
                    , TIPOPROD = :TIP_PROD
                    , PAIS = :PAIS
                    , COD_IMPUESTO = :COD_IMPUESTO
                    , IMPUESTO = :IMPUESTO
                    WHERE CODSAP = :CODPRO1";

            DB::connection('oracle_UnificadoQA')->statement($sql,[
                'TIPOPROD' => $producto->tipoProd,
                'AFECTO' => $afecto,
                'DESPRO' => $producto->desLarga,
                'DESPROCORTA' => $producto->desCorta,
                'DESPROLARGA' => $producto->desLarga,
                'CODJER' => $producto->codJerarquia,
                'CODLIN' => $producto->codLinea,
                'DESLIN' => $producto->deslinea,
                'CODSEC' => $producto->codSeccion,
                'DESSEC' => $producto->desSeccion,
                'CODRUB' => $producto->codRubro,
                'DESRUB' => $producto->desRubro,
                'CODSUBGRU' => $producto->codSubRubro,
                'DESSUBGRU' => $producto->desSubRubro,
                'CODGRU' => $producto->codGrupo,
                'DESGRU' => $producto->desGrupo,
                'CODMAR' => $producto->codMarca,
                'DESMAR' => $producto->desMarca,
                'PESO' => $producto->pesoBruto,
                'PESO_NETO' => $producto->pesoNeto,
                'UNI_PESO' => $producto->uniPeso,
                'VOLUMEN' => ($producto->volumen / 1000000),
                'UNI_VOLUMEN' => $producto->uniVolumen,
                'CODUNI' => $producto->uniBase,
                'CODPRO' => $producto->codMaterial,
                'UNI_EMBALAJE' => $producto->uniEmbalaje,
                'DEN_CONV_EMB' => $producto->denConvEmb,
                'EMBALAJE' => $producto->embalaje,
                'PESO_NETO_EMB' => ($producto->pesoNetoEmbalaje * 1),
                'PESO_EMB' => ($producto->pesoBrutoEmbalaje * 1),
                'UNI_SUB_EMB' => $producto->uniSubEmbalaje,
                'DEN_CONV_SUB_EMB' => $producto->denConvSubEmb,
                'SUB_EMBALAJE' => $producto->subEmbalaje,
                'PESO_NETO_SUB_EMB' => ($producto->pesoNetoSubEmb * 1),
                'PESO_BRUTO_SUB_EMB' => ($producto->pesoBrutoSubEmb / 100),
                'VOLUMEN_EMB' => ($producto->VolumenEmb / 100),
                'UNI_PESO_EMB' => $producto->uniPesoEmb,
                'UNI_VOLUMEN_EMB' => $producto->uniVolumenEmb,
                'VOLUMEN_SUB_EMB' => $producto->volumenSubEmb,
                'UNI_PESO_SUB_EMB' => $producto->uniPesoSubEmb,
                'UNI_VOLUMEN_SUB_EMB' => $producto->uniVolumenSubEmb,
                'INDSBR' => $sobreVenta,
                'TIPROT' => $centro->rotacion,
                'MARCA_PROPIA' => $marca_propia,
                'IMPORTADO' => $importado,
                'CATEGORIA' => $sociedad->catComision,
                'COD_NEGOCIO' => $codneg,
                'TIP_PROD' => $tipoprod,
                'PAIS' => $pais,
                'COD_IMPUESTO' => $impEspecifico,
                'IMPUESTO' => $clasifFiscal,
                'CODPRO1' => $producto->codMaterial
            ]);

            DB::connection('oracle_UnificadoQA')->statement("commit");

        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la ma_costosxcentros ------------------------------------------------------------------------------------------------------
        foreach ($producto->sociedades as $soc)
        {
            if ($soc->orgVentas == 2000) {
                foreach ($producto->centros as $centroSap)
                {
                    $sql = "select codpro from ma_costosxcentros
                            where centro = :centro
                            and codpro = :codpro
                            and canal = :canal";

                    $resultado = DB::connection('oracle_UnificadoQA')->select($sql, [
                        'codpro' => $codigo,
                        'centro' => $centroSap->codCentro,
                        'canal' => $soc->canalDist
                    ]);
            
                    if(empty($resultado)){
                        $sql = "insert into ma_costosxcentros (codemp, centro, codpro, cosprom, canal, fecing) 
                                values(3, :centro, :codpro, :cosprom, :canal, sysdate)";
            
                            DB::connection('oracle_UnificadoQA')->statement($sql,[
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'cosprom' => $soc->costoMedioVariable,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_UnificadoQA')->statement("commit");

                    }else{
                        $sql = "update ma_costosxcentros set cosprom = :cosprom
                                where centro = :centro
                                and codpro = :codpro
                                and canal = :canal";
            
                            DB::connection('oracle_UnificadoQA')->statement($sql,[
                                'cosprom' => $soc->costoMedioVariable,
                                'centro' => $centroSap->codCentro,
                                'codpro' => $codigo,
                                'canal' => $soc->canalDist
                            ]);
            
                            DB::connection('oracle_UnificadoQA')->statement("commit");

                    }    
                }
            }
        }

        //  ---------------------------------------------------------------------------------------------------------   Inserccion en la re_codebar ----------------------------------------------------------------------------------------------------------------
        if ($producto->UMB = "") {
            $sql = "select codpro from re_codebar where codpro = :codpro and codemp = 3";

            $resultado = DB::connection('oracle_UnificadoQA')->select($sql, [
                'codpro' => $codigo,
            ]);

            if(empty($resultado)){
                $sql = "insert into re_codebar (CODPRO,CODBAR,CODEST,FECHA) 
                        values(:codpro, :codbar, 0, sysdate)";
    
                    DB::connection('oracle_UnificadoQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_UnificadoQA')->statement("commit");
            }else{
                $sql = "update re_codebar set codbar = :codbar 
                        where codpro = :codpro";
    
                    DB::connection('oracle_UnificadoQA')->statement($sql,[
                        'codpro' => $codigo,
                        'codbar' => $producto->UMB
                    ]);
    
                    DB::connection('oracle_UnificadoQA')->statement("commit");
            } 
        }

        return 'ok';
    }
}

 ?>