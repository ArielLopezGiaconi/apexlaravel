<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 18:13
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\DeCliente;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class MaDerechosPDO extends Model
{
    public static function usuariosConDerecho($codder, $codemp = 3)
    {
        $arrayReturn = null;
        $sql = "SELECT CODUSU, :codder CODDER FROM RE_USUADER WHERE CODDER = :codder AND codemp = :cod_emp";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codder' => $codder,
            'cod_emp' => $codemp,
        ]);

        return $resultado;
    }

    public static function usuariosSinDerecho($codder, $codemp = 3)
    {
        $arrayReturn = null;
        $sql = "select distinct a.userid usuario, :codder codder from ma_usuario a, re_usuader b
                where a.codest = 0
                  and a.codemp = :cod_emp
                  and a.codemp = b.codemp
                  and a.userid = b.codusu
                  and b.codder not in (:codder)
                  group by a.userid";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codder' => $codder,
            'cod_emp' => $codemp,
        ]);

        return $resultado;
    }

    public static function getDerecho($codder)
    {
        $arrayReturn = null;
        $sql = "select descri from ma_Derecho where codder = :codder";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codder' => $codder
        ]);

        return $resultado;
    }

    public static function getUsuarioConRut($rutusu)
    {
        $arrayReturn = null;
        $sql = "select USERID, RUTUSU, NOMBRE || ' ' || APEPAT || ' ' || APEMAT NOMBRE from ma_usuario
                where rutusu = :rutusu";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutusu' => $rutusu
        ]);

        return $resultado;
    }

    public static function getUsuarioConUsuario($usuario)
    {
        $arrayReturn = null;
        $sql = "select USERID, RUTUSU, NOMBRE || ' ' || APEPAT || ' ' || APEMAT AS NOMBRE, nvl(mail01,'Sin Correo') as correo, nvl(anexos, '0000') as anexo, 'Sin Perfil' as perfil from ma_usuario
                where userid = :usuario";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'usuario' => $usuario
        ]);

        return $resultado;
    }

    public static function getDerechoUsuario($usuario)
    {
        $arrayReturn = null;
        $codemp = 3;
        $sql = "select a.codder, substr(a.descri,0,50) descri, a.system from ma_Derecho a, re_usuader b
                where b.codusu = :usuario
                  and a.codder = b.codder
                  and b.codemp = :cod_emp";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'usuario' => $usuario,
            'cod_emp' => $codemp,
        ]);

        return $resultado;
    }

    public static function getDerechoUsuarioNoTiene($usuario)
    {
        $arrayReturn = null;
        $codemp = 3;
        $sql = "select codder, substr(a.descri,0,50) descri, system from ma_Derecho a
                where not exists (select 1 from re_usuader x where x.codusu = :usuario and x.codder = a.codder and codemp = :cod_emp)";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'usuario' => $usuario,
            'cod_emp' => $codemp,
        ]);

        return $resultado;
    }
    //----------------------------------------------- Sentencias de Modificacion ------------------------------------------------------------

    public static function setQuitarDerecho($usuario, $derecho)
    {
        $arrayReturn = null;
        $usuario = trim($usuario);
        $derecho = trim($derecho);
        $delete = "DELETE RE_USUADER WHERE CODDER = :codder AND CODUSU = :codusu";
        DB::connection('oracle_dmventas')->statement($delete,[ 
            'CODDER' => $derecho,
            'CODUSU' => $usuario
            ]);
        DB::connection('oracle_dmventas')->statement("commit");
        
        // $sql = "select codusu, codder from re_usuader where codder = :derecho and codusu = :usuario";
        // $resultado = DB::connection('oracle_dmventas')->select($sql, [
        //     'derecho' => $derecho,
        //     'usuario' => $usuario
        // ]);

        return 'OK';
        // return $resultado;
    }

    public static function setQuitarTodosLosDerechos($derecho)
    {
        $arrayReturn = null;
        $derecho = trim($derecho);
        $delete = "DELETE RE_USUADER WHERE CODDER = :codder AND CODEMP = 3";
        DB::connection('oracle_dmventas')->statement($delete,[ 
            'CODDER' => $derecho
            ]);
        DB::connection('oracle_dmventas')->statement("commit");

        return 'OK';
    }

    public static function setLimpiaDerechos($codusu)
    {
        $arrayReturn = null;
        $codusu = trim($codusu);
        $delete = "DELETE RE_USUADER WHERE codusu = :codusu";
        DB::connection('oracle_dmventas')->statement($delete,[ 
            'codusu' => $codusu
            ]);
        DB::connection('oracle_dmventas')->statement("commit");

        return 'OK';
    }

    public static function setIgualaDerechos($usuario, $principal)
    {
        $arrayReturn = null;
        $usuario = trim($usuario);
        $principal = trim($principal);
        $insert = "insert into re_usuader (codusu, codder, codemp)
                    select :codusu usuario, codder, codemp from re_usuader
                                    where codemp = 3
                                    and codusu = :principal";
                    DB::connection('oracle_dmventas')->statement($insert,[ 
                        'codusu' => $usuario,
                        'principal' => $principal            
                        ]);
        DB::connection('oracle_dmventas')->statement("commit");

        return 'OK';
    }

    public static function setDarDerecho($codusu, $codder)
    {
        $arrayReturn = null;
        $codusu = trim($codusu);
        $codder = trim($codder);
        $insert = "INSERT INTO RE_USUADER (CODUSU, CODDER, CODEMP) VALUES (:codusu, :codder, 3)";
        DB::connection('oracle_dmventas')->statement($insert,[ 
            'codusu' => $codusu,
            'codder' => $codder            
            ]);
        DB::connection('oracle_dmventas')->statement("commit");

        return 'OK';
    }

    public static function setDarTodosDerechos($codder)
    {
        $arrayReturn = null;
        $codder = trim($codder);
        $insert = "insert into re_usuader (codusu, codder, codemp)
                    select distinct a.userid usuario, :codder codder, a.codemp from ma_usuario a, re_usuader b
                                    where a.codest = 0
                                    and a.codemp = 3
                                    and a.codemp = b.codemp
                                    and a.userid = b.codusu
                                    and b.codder not in (:codder2)
                    group by a.userid, a.codemp";
                    DB::connection('oracle_dmventas')->statement($insert,[ 
                        'codder' => $codder,
                        'codder2' => $codder            
                        ]);
        DB::connection('oracle_dmventas')->statement("commit");

        return 'OK';
    }
}