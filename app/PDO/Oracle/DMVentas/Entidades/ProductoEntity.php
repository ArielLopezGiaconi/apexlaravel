<?php
/**
 * Created by PhpStorm.
 * User: IGONZALEZ
 * Date: 12/03/2019
 * Time: 17:01
 */

namespace App\PDO\Oracle\DMVentas\Entidades;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use DB;

class ProductoEntity implements \JsonSerializable {

    private $codigo_producto;
    private $precio;
    private $precio_min;
    private $costo_trans;
    private $origen;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getCodigoProducto()
    {
        return $this->codigo_producto;
    }

    /**
     * @param mixed $codigo_producto
     */
    public function setCodigoProducto($codigo_producto)
    {
        $this->codigo_producto = $codigo_producto;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $rut_cliente
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    public function getPrecioMin()
    {
        return $this->precio_min;
    }

    /**
     * @param mixed $rut_cliente
     */
    public function setPrecioMin($precio_min)
    {
        $this->precio_min = $precio_min;
    }

    /**
     * @return mixed
     */
    public function getCostoTrans()
    {
        return $this->costo_trans;
    }

    /**
     * @param mixed $costo
     */
    public function setCostoTrans($costo_trans)
    {
        $this->costo = $costo_trans;
    }

    /**
     * @return mixed
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * @param mixed $precio
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;
    }

    public function jsonSerialize()
    {
        return [
            "codpro" => $this -> getCodigoProducto(),
            "precio" => $this -> getPrecio(),
            "precio_min" => $this -> getPrecioMin(),
            "origen" => $this -> getOrigen(),
            "costo_trans" => $this -> getCostoTrans()
        ];
    }
}