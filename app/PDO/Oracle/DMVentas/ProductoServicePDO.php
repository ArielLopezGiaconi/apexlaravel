<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 18:13
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\DeCliente;
use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Support\Collection;
use Monolog\Handler\IFTTTHandler;

class ProductoServicePDO extends Model
{
    public static function ValidaDireccion($coddir)
    {

        $sql = "select codval, desval destino, decode(codval, 292, 'HUB','NORMAL') condicion, decode(codref, 130, 'SANTIAGO', 'REGIONES') cobro 
            from de_dominio
            where coddom = 2
            and codval = :coddir";
            
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'coddir' => $coddir,
        ]);

        if(!$resultado){
            $resultado = "0";
        }

        return $resultado;
    }
    
    public static function ValidaTablasProducto($codpro)
    {
        $resultado = "";
        $sql = "";
        //----------------------------------------------------VALIDA TABLAS PRODUCTO---------------------------------------------------
        $sql = "select codpro from re_Canprod
        where codpro = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
        ]);

        if(empty($resultado)){
            return "El producto no esta ingresado en la tablas de precios.";
        }

        $sql = "select codpro, getdominio(20, estpro) estado, estpro from ma_product
        where codpro = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
        ]);

        $estado = $resultado[0]->estado;
        $estpro = $resultado[0]->estpro;

        //dd($estpro);
        if($estpro <> 1){
            if($estpro <> 11){
                if($estpro <> 3){
                    if($estpro <> 2){
                        if($estpro <> 5){
                            if($estpro <> 8){
                                return "No puede consultar producto esta con estado => " . $estado;
                            }
                        } 
                    } 
                } 
            } 
        } 
                
        return "OK";
    }

    public static function ValidaConvenio($codpro, $rutcli)
    {
        $resultado = "";
        $sql = "";
        //----------------------------------------------------VALIDA CONVENIO---------------------------------------------------
        $sql = "select a.rutcli, b.codpro, b.precio, '0' precio_min, 'CONVENIO' Origen, 0 costo_trans from en_conveni a, de_conveni b
                where a.rutcli = :rutcli
                  and trunc(a.fecven) >= trunc(sysdate)
                  and a.numcot = b.numcot
                  and b.codpro = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
            'rutcli' => $rutcli,
        ]);

        if(!empty($resultado)){
            return $resultado;
        }

        return "0";
    }

    public static function ValidaConvenioMarco($codpro, $rutcli)
    {
        //----------------------------------------------------VALIDA CONVENIO MARCO---------------------------------------------------
        $sql = "select GETCLASIFICACION_CLIENTE(3, :rutcli) segmento from dual";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
        ]);
                
        //dd($resultado[0]->segmento);
        $segmento = $resultado[0]->segmento;

        if($segmento == "GOBIERNO"){
            $sql = "select :rutcli rutcli, b.codpro, b.precio, '0' precio_min, 'CONVENIO MARCO' Origen, 0 costo_trans from en_conveni a, de_conveni b
            where a.rutcli = 78912345
                and trunc(a.fecven) >= trunc(sysdate)
                and a.numcot = b.numcot
                and b.codpro = :codpro";

            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'codpro' => $codpro,
                'rutcli' => $rutcli,
            ]);

            if(!empty($resultado)){
                return $resultado;
            }
        }

        return "0";
    }

    public static function ValidaCotizacion($codpro, $rutcli)
    {
        // ----------------------------------------------------VALIDA COTIZACION---------------------------------------------------
        $sql = "select a.rutcli, b.codpro, b.precio, '0' precio_min, 'COTIZACION' Origen, 0 costo_trans FROM EN_COTIZAC A, DE_COTIZAC B
        where A.RUTCLI = :rutcli
        AND TRUNC(A.FECVEN) >= TRUNC(SYSDATE)
        AND A.NUMCOT = B.NUMCOT
        AND B.CODPRO = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
        'codpro' => $codpro,
        'rutcli' => $rutcli,
        ]);

        if(!empty($resultado)){
        return $resultado;
        }

        return "0";

    }

    public static function ValidaCostoEspecial($codpro, $rutcli)
    {
        // ----------------------------------------------------VALIDA COSTO ESPECIAL---------------------------------------------------
        $sql = "select a.codpro, a.precio, nvl(a.prefij, 0) precio_min, 'PRECIO DE LISTA' Origen, 0 costo_trans from re_canprod a, (
            select a.rutcli, a.codcnl, a.codemp, b.codpro, b.costo from re_cliente_lista_linea a, ma_product b, re_claprod c
            where a.codemp = 3
            and a.rutcli = :rutcli
            and b.codpro = :codpro
            and a.codlin = b.codlin
            and c.codpro = b.codpro
            and c.codgen = 246
            and a.codcat = c.codclasifica) b
        where a.codemp = b.codemp
            and a.codpro = b.codpro
            and a.codcnl = b.codcnl";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
            'rutcli' => $rutcli,
        ]);
        
        $precio_min = $resultado[0]->precio_min;

        $sql = "select rutcli, codpro, precio, :precio_min precio_min, 'COSTO ESPECIAL' Origen, 0 costo_trans from en_cliente_costo
                where rutcli = :rutcli
                    and trunc(fecfin) >= trunc(sysdate)
                    and codpro = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
            'rutcli' => $rutcli,
            'precio_min' => $precio_min,
        ]);

        if(!empty($resultado)){
            return $resultado;
        }

        return "0";

    }

    public static function ValidaPrecioLista($codpro, $rutcli)
    {
        // ----------------------------------------------------PRECIO DE LISTA---------------------------------------------------
        $sql = "select a.codpro, a.precio, nvl(a.prefij, 0) precio_min, 'PRECIO DE LISTA' Origen, 0 costo_trans from re_canprod a, (
            select a.rutcli, a.codcnl, a.codemp, b.codpro, b.costo from re_cliente_lista_linea a, ma_product b, re_claprod c
            where a.codemp = 3
            and a.rutcli = :rutcli
            and b.codpro = :codpro
            and a.codlin = b.codlin
            and c.codpro = b.codpro
            and c.codgen = 246
            and a.codcat = c.codclasifica) b
        where a.codemp = b.codemp
            and a.codpro = b.codpro
            and a.codcnl = b.codcnl";
                  
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
            'rutcli' => $rutcli,
        ]);

        if(!empty($resultado)){
            return $resultado;
        }

        if(!$resultado){
            $resultado = array([
                "codpro" => $codpro,
                "precio" => "Sin resultado",
                "precio_min" => "Sin resultado",
                "Origen" => "Sin resultado",
                "costo_trans" => "Sin Resultado",
                ]
            );

            return $resultado;
        }

        return "0";

    }

    public static function ObtieneCostoTrans($kilos, $coddir)
    {
        $sql = "select codcom, kilos_desde, kilos_hasta, DECODE (tipo_valor, 1, 'TRAMO', 'KILO') cobro, valor valor_tramo, (valor * :kilo) valor_kilo
        FROM ma_tarifa_despacho a
        WHERE codemp = 3 
          AND codtra = 1 
          AND sucursal = 0
          and codcom = :coddir
          and :kilos between kilos_desde and kilos_hasta
        ORDER BY codcom, kilos_desde";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'kilo' => round($kilos, 0),
            'coddir' => $coddir,
            'kilos' => round($kilos, 0),
        ]);

        $cobro = $resultado[0]->cobro;

        if($cobro == "KILO"){
            $costo_trans = $resultado[0]->valor_kilo;
        }else{
            $costo_trans = $resultado[0]->valor_tramo;
        }

        return $costo_trans;

    }

    // public static function ObtieneCostoTrans($codpro, $cantid, $coddir, $rutcli)
    // {
    //     $sql = "select codpro, despro, (nvl(peso,0) * :cantid) kilos from ma_product
    //     where codpro = :codpro";
                  
    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'cantid' => $cantid,
    //     ]);

    //     $kilos = $resultado[0]->kilos;

    //     if($kilos < 1){
    //         $kilos = 1;
    //     }else{
    //         $kilos = bcdiv($kilos,1,0);
    //     }

    //     if($kilos == 0){
    //         return "sin peso";
    //     }

    //     $sql = "select codcom from re_ddescli
    //     where rutcli = :rutcli
    //     and coddir = :coddir";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'rutcli' => $rutcli,
    //         'coddir' => $coddir,
    //     ]);

    //     $codcom = $resultado[0]->codcom;

    //     $sql = "select codcom, kilos_desde, kilos_hasta, DECODE (tipo_valor, 1, 'TRAMO', 'KILO') cobro, valor valor_tramo, (valor * :kilo) valor_kilo
    //     FROM ma_tarifa_despacho a
    //     WHERE codemp = 3 
    //       AND codtra = 1 
    //       AND sucursal = 0
    //       and codcom = :codcom
    //       and :kilos between kilos_desde and kilos_hasta
    //     ORDER BY codcom, kilos_desde";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codcom' => $codcom,
    //         'kilos' => $kilos,
    //         'kilo' => $kilos,
    //     ]);

    //     $cobro = $resultado[0]->cobro;

    //     if($cobro == "KILO"){
    //         $costo_trans = $resultado[0]->valor_kilo;
    //     }else{
    //         $costo_trans = $resultado[0]->valor_tramo;
    //     }

    //     return $costo_trans;

    // }

    // public static function Precio($codpro, $rutcli)
    // {
    //     $resultado = "";
    //     $sql = "";
    //     //----------------------------------------------------VALIDA CONVENIO---------------------------------------------------
    //     $sql = "select a.rutcli, b.codpro, b.precio, '0' precio_min, 'CONVENIO' Origen from en_conveni a, de_conveni b
    //             where a.rutcli = :rutcli
    //               and trunc(a.fecven) >= trunc(sysdate)
    //               and a.numcot = b.numcot
    //               and b.codpro = :codpro";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'rutcli' => $rutcli,
    //     ]);

    //     if(!empty($resultado)){
    //         return $resultado;
    //     }

       
    //     //----------------------------------------------------VALIDA CONVENIO MARCO---------------------------------------------------
    //     $sql = "select GETCLASIFICACION_CLIENTE(3, :rutcli) segmento from dual";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'rutcli' => $rutcli,
    //     ]);
               
    //     //dd($resultado[0]->segmento);
    //     $segmento = $resultado[0]->segmento;

    //     if($segmento == "GOBIERNO"){
    //         $sql = "select :rutcli rutcli, b.codpro, b.precio, '0' precio_min, 'CONVENIO MARCO' Origen from en_conveni a, de_conveni b
    //         where a.rutcli = 78912345
    //           and trunc(a.fecven) >= trunc(sysdate)
    //           and a.numcot = b.numcot
    //           and b.codpro = :codpro";
    //         $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //             'codpro' => $codpro,
    //             'rutcli' => $rutcli,
    //         ]);
    //         if(!empty($resultado)){
    //             return $resultado;
    //         }
    //     }

    //     // ----------------------------------------------------VALIDA COTIZACION---------------------------------------------------
    //     $sql = "select a.rutcli, b.codpro, b.precio, '0' precio_min, 'COTIZACION' Origen FROM EN_COTIZAC A, DE_COTIZAC B
    //             where A.RUTCLI = :rutcli
    //               AND TRUNC(A.FECVEN) >= TRUNC(SYSDATE)
    //               AND A.NUMCOT = B.NUMCOT
    //               AND B.CODPRO = :codpro";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'rutcli' => $rutcli,
    //     ]);

    //     if(!empty($resultado)){
    //         return $resultado;
    //     }

    //     // ----------------------------------------------------VALIDA COSTO ESPECIAL---------------------------------------------------
    //     $sql = "select a.codpro, a.precio, a.prefij precio_min, 'PRECIO DE LISTA' Origen from re_canprod a, (
    //                 select a.rutcli, a.codcnl, a.codemp, b.codpro, b.costo from re_cliente_lista_linea a, ma_product b, re_claprod c
    //                 where a.codemp = 3
    //                 and a.rutcli = :rutcli
    //                 and b.codpro = :codpro
    //                 and a.codlin = b.codlin
    //                 and c.codpro = b.codpro
    //                 and c.codgen = 246
    //                 and a.codcat = c.codclasifica) b
    //             where a.codemp = b.codemp
    //               and a.codpro = b.codpro
    //               and a.codcnl = b.codcnl";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'rutcli' => $rutcli,
    //     ]);
        
    //     $precio_min = $resultado[0]->precio_min;

    //     $sql = "select rutcli, codpro, precio, :precio_min precio_min, 'COSTO ESPECIAL' Origen from en_cliente_costo
    //             where rutcli = :rutcli
    //                 and trunc(fecfin) >= trunc(sysdate)
    //                 and codpro = :codpro";

    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'rutcli' => $rutcli,
    //         'precio_min' => $precio_min,
    //     ]);

    //     if(!empty($resultado)){
    //         return $resultado;
    //     }

    //     // ----------------------------------------------------PRECIO DE LISTA---------------------------------------------------

    //     $sql = "select a.codpro, a.precio, a.prefij precio_min, 'PRECIO DE LISTA' Origen from re_canprod a, (
    //                 select a.rutcli, a.codcnl, a.codemp, b.codpro, b.costo from re_cliente_lista_linea a, ma_product b, re_claprod c
    //                 where a.codemp = 3
    //                 and a.rutcli = :rutcli
    //                 and b.codpro = :codpro
    //                 and a.codlin = b.codlin
    //                 and c.codpro = b.codpro
    //                 and c.codgen = 246
    //                 and a.codcat = c.codclasifica) b
    //             where a.codemp = b.codemp
    //               and a.codpro = b.codpro
    //               and a.codcnl = b.codcnl";
                  
    //     $resultado = DB::connection('oracle_dmventas')->select($sql, [
    //         'codpro' => $codpro,
    //         'rutcli' => $rutcli,
    //     ]);

    //     if(!empty($resultado)){
    //         return $resultado;
    //     }

    //     if(!$resultado){
    //         $resultado = array([
    //             "codpro" => $codpro,
    //             "precio" => "Sin resultado",
    //             "precio_min" => "Sin resultado",
    //             "Origen" => "Sin resultado",
    //             ]
    //         );

    //         return $resultado;
    //     }
    // }

    public static function ValidaCliente($rut)
    {

        $sql = "select 1 from en_cliente where rutcli = :rutcli";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rut,
        ]);

        if(!$resultado){
            $resultado = "0";
        }

        return $resultado;
    }

    public static function ValidaCoddir($rut, $coddir)
    {

        $sql = "select 1 from re_ddescli where rutcli = :rutcli and coddir = :coddir";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rut,
            'coddir' => $coddir,
        ]);

        if(!$resultado){
            $resultado = "0";
        }

        return $resultado;
    }

    public static function ValidaProducto($codpro)
    {
        $sql = "select 1 from ma_product where codpro = :codpro";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
        ]);

            if(!$resultado){
                $resultado = "0";
            }
        
        return $resultado;
    }
}