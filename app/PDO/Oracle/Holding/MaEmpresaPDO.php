<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 9:31
 */

namespace App\PDO\Oracle\Holding;

use App\Entities\Oracle\Holding\MaEmpresa;
use DB;
use Illuminate\Database\Eloquent\Model;

class MaEmpresaPDO extends Model
{
    public static function existsCompanyByCod($in_codemp)
    {
        $sql = "SELECT COUNT(*) AS existe FROM ma_empresa"
            . " WHERE codemp = :cod_emp";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_emp' => $in_codemp
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getCompanyByCod($in_codemp, $first_row = true)
    {
        $sql = "SELECT * FROM ma_empresa"
            . " WHERE codemp = :cod_emp";

        $resultado = DB::connection('oracle_unificado')->select($sql, [
            'cod_emp' => $in_codemp
        ]);

        return MaEmpresaPDO::returnFormat($resultado, $first_row);
    }

    private static function returnFormat($registros, $first_row = true) {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $empresa = new MaEmpresa($registro);
            if($first_row) {
                return $empresa;
            }

            $arrayReturn[] = $empresa;
        }
        return $arrayReturn;
    }
}
