<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 11:03
 */

namespace App\PDO\Oracle\Holding;

use App\Entities\Oracle\Holding\MaProduct;
use \DB;
use Illuminate\Database\Eloquent\Model;

class MaProductPDO extends Model
{
    public static function existsProductByCod($in_codpro)
    {
        $sql = "SELECT COUNT(*) AS existe FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function sapGetMaterialProdByCod($in_codpro)
    {
        $sql = "SELECT codsap FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) > 0) ? $resultado[0]->codsap : null;
    }

    public static function getProductByCod($in_codpro, $first_row = true) {
        $sql = "SELECT * FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return MaProductPDO::returnProductFormat($resultado, true);
    }

    public static function getProductByMaterial($in_material) {
        $sql = "SELECT * FROM ma_product WHERE codsap = :cod_material";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_material' => intval($in_material)
        ]);

        return MaProductPDO::returnProductFormat($resultado, true);
    }

    public static function getAllSapProductsChile($in_codemp)
    {
        $sql = "SELECT a.codpro, a.codsap FROM ma_product a WHERE a.codsap IS NOT NULL";
        if ($in_codemp == 3) {
            $resultado = DB::connection('oracle_dmventas')->select($sql);
        } else {
            $sql .= " AND EXISTS ( "
                . " SELECT 1 FROM ma_empprod b"
                . " WHERE b.codpro = a.codpro"
                . " AND b.codemp = :cod_emp"
                . " )";

            $resultado = DB::connection('oracle_unificado')->select($sql, [
                'cod_emp' => $in_codemp
            ]);
        }
        return MaProductPDO::returnProductFormat($resultado);
    }

    private static function returnProductFormat($registros, $first_row = false) {
        $arrayReturn = null;
        foreach($registros as $registro) {
            $product = new MaProduct($registro);
            if($first_row) {
                return $product;
            }
            $arrayReturn[] = $product;
        }

        return $arrayReturn;
    }
}
