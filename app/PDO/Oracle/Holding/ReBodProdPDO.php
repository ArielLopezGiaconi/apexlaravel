<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 11:56
 */

namespace App\PDO\Oracle\Holding;

use App\Entities\Oracle\DMVentas\EnCliente;
use App\Entities\Oracle\DMVentas\ReBodProdDimerc;
use App\PDO\MySql\Magento\EmailCobranzaPDO;
use App\PDO\MySql\Magento\SalesFlatOrderPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Entities\Oracle\DMVentas\NotaVenta;
// use Illuminate\Support\Facades\DB;

class ReBodProdPDO extends Model
{
    public static function existsStockProduct($in_codemp, $in_codbod, $in_codpro)
    {
        $conexion = ($in_codemp == 3) ? 'oracle_dmventas' : 'oracle_unificado';
        $table = ($in_codemp == 3 ) ? 're_bodprod_dimerc' : 're_bodprod';

        $sql = "SELECT COUNT(*) AS existe FROM ".$table
            . " WHERE codpro = :cod_prod"
            . " AND codbod = :cod_bod"
            . " AND codemp = :cod_emp";

        $resultado = DB::connection($conexion)->select($sql, [
            'cod_prod' => $in_codpro,
            'cod_bod' => $in_codbod,
            'cod_emp' => $in_codemp,
        ]);

        return ($resultado && $resultado[0]->existe > 0) ? true : false;
    }


    private static function returnStockFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $bodega = new ReBodProdDimerc($registro);
            if ($first_row) {
                return $bodega;
            }

            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }
}