<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 25-01-2018
 * Time: 14:40
 */

namespace App\PDO\Oracle\DMTransferWeb;

use App\Entities\Oracle\Holding\SapFboTfStock;
use App\Entities\Oracle\DMTrasferWeb\TFConfiguracion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class SapFboTfStockPDO extends Model
{
    public static function getCredencialesCorreoDimerc()
    {
        $sql = "select id_sitio, nombre, user_ftp, pass_ftp, server_ftp, user_mail, pass_mail, "
            . " server_mail, lista_precio_base, observacion, "
            . " server_mail as host, observacion as port "
            . " from tf_configuracion "
            . " where id_sitio = 6";

        $resultado = DB::connection('oracle_dmtransferweb')->select($sql);
        $arrayReturn = SapPdo::returnRegistrosComprasFormat($resultado);
        return $arrayReturn;
    }

    public static function insertOrUpdateStock(SapFboTfStock $in_stock)
    {
        $sql = "MERGE INTO SAP_FBO_TF_STOCK a"
            . "     USING(SELECT :cod_emp codemp, :cod_bod codbod, :cod_pro codpro , :stock_fisico stkfis , :stock_compr stkcom, :stock_disp stkdis, :cost_prom cosprom, :pre_existe pre_existe FROM dual) src"
            . " ON(a.codemp = src.codemp AND a.codbod = src.codbod AND a.codpro = src.codpro)"
            . " WHEN MATCHED THEN"
            . "    UPDATE SET"
            . "    a.stkfis = src.stkfis,"
            . "    a.stkcom = src.stkcom,"
            . "    a.stkdis = src.stkdis,"
            . "    a.cosprom = src.cosprom,"
            . "    a.fecha_creac = SYSDATE,"
            . "    a.fecha_proce = null,"
            . "    a.pre_existe = src.pre_existe,"
            . "    a.estado = 0"
            . " WHEN NOT MATCHED THEN"
            . "     INSERT(codemp, codbod, codpro, stkfis, stkcom, stkdis, fecha_creac, fecha_proce, estado, pre_existe)"
            . "     VALUES(src.codemp, src.codbod, src.codpro, src.stkfis, src.stkcom, src.stkdis, SYSDATE, null, 0, src.pre_existe)";

        DB::connection('oracle_unificado')->statement($sql, [
            'stock_fisico' => $in_stock->getStkfis(),
            'stock_compr' => $in_stock->getStkcom(),
            'stock_disp' => $in_stock->getStkfis(),
            'cost_prom' => $in_stock->getCosprom(),
            'cod_pro' => $in_stock->getCodpro(),
            'cod_bod' => $in_stock->getCodbod(),
            'cod_emp' => $in_stock->getCodemp(),
            'pre_existe' => $in_stock->getPreExiste(),
        ]);

        return true;
    }

    public static function insertStock(SapFboTfStock $in_stock)
    {
        $sql = " INSERT INTO SAP_FBO_TF_STOCK (codemp, codbod, codpro, stkfis, stkcom, stkdis, cosprom, fecha_creac, fecha_proce, estado, pre_existe)"
            . " VALUES (:cod_emp, :cod_bod, :cod_pro, :stock_fisico, :stock_compr, :stock_disp, :cost_prom, SYSDATE, NULL, 0, :pre_existe)"
        ;

        DB::connection('oracle_unificado')->statement($sql, [
            'stock_fisico' => $in_stock->getStkfis(),
            'stock_compr' => $in_stock->getStkcom(),
            'stock_disp' => $in_stock->getStkfis(),
            'cost_prom' => $in_stock->getCosprom(),
            'cod_pro' => $in_stock->getCodpro(),
            'cod_bod' => $in_stock->getCodbod(),
            'cod_emp' => $in_stock->getCodemp(),
            'pre_existe' => $in_stock->getPreExiste(),
        ]);

        return true;
    }

    public static function deleteStock($in_codemp, $in_codbod)
    {
        $sql = "DELETE FROM SAP_FBO_TF_STOCK WHERE codemp = :cod_emp AND codbod = :cod_bod";
        DB::connection('oracle_unificado')->statement($sql, [
            'cod_emp' => $in_codemp,
            'cod_bod' => $in_codbod
        ]);

        return true;
    }

    public static function migrateTfStock($in_codemp) {
        try {
            $sql = "CALL SAP_PUT_ACTUALIZA_STOCK(:cod_emp)";
            DB::connection('oracle_unificado')->statement($sql, [
                'cod_emp' => $in_codemp
            ]);

            return true;
        }
        catch(\Exception $e) {
            return false;
        }
    }

    public static function returnFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $stock = new SapFboTfStock($registro);
            if($first_row) {
                return $stock;
            }
            $arrayReturn[] = $stock;
        }

        return $arrayReturn;
    }
}