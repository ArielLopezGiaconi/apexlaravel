<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 04-02-2019
 * Time: 16:53
 */

namespace App\PDO\Oracle\DMTransferWeb;

use App\Entities\Oracle\DMTrasferWeb\TFConfiguracion;
use App\PDO\MySql\Magento\EmailCobranzaPDO;
use App\PDO\MySql\Magento\SalesFlatOrderPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use PhpParser\Node\Stmt\Foreach_;

class TmpFboDmgProductsPDO extends Model
{
    public static function createTemporalTable(array $codpros)
    {
        // 0.- Delete table if exists
        TmpFboDmgProductsPDO::dropTemporalTable();

        // 1.- Create table
        $sql = "CREATE TABLE tmp_fbo_dimeiggs_products ("
            . " CODPRO VARCHAR2(10)"
            . ")";

        DB::connection('oracle_dmtransferweb')->statement($sql);

        // 2.- Insert All data
        $query = null;
        foreach ($codpros as $codpro) {
            if ($query != null) {
                $query .= " UNION ALL";
            }

            $query .= " SELECT '" . $codpro . "' FROM DUAL";

        }
        DB::connection('oracle_dmtransferweb')
            ->statement('INSERT INTO tmp_fbo_dimeiggs_products (codpro)' . $query);// ->insert($codpro)
        ;

        return true;
    }

    public static function dropTemporalTable()
    {
        $sql = "BEGIN EXECUTE IMMEDIATE 'DROP TABLE tmp_fbo_dimeiggs_products'; EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942 THEN RAISE; END IF; END;";
        DB::connection('oracle_dmtransferweb')->statement($sql);

        return true;
    }
}