<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 11:56
 */

namespace App\PDO\Oracle\DMVentasPeru;

use App\Entities\Oracle\DMVentas\EnCliente;
use App\Entities\Oracle\DMVentas\ReBodProdDimerc;
use App\PDO\MySql\Magento\EmailCobranzaPDO;
use App\PDO\MySql\Magento\SalesFlatOrderPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Entities\Oracle\DMVentas\NotaVenta;

class ReBodProdDimercPDO extends Model
{
    public static function getStockProduct($first_row = false, $in_codpro, $in_codbod, $in_codemp)
    {
        $sql = "SELECT * FROM re_bodprod_dimerc "
            . " WHERE codpro = :cod_prod"
            . " AND codbod = :cod_bod"
            . " AND codemp = :cod_emp";

        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro,
            'cod_bod' => $in_codbod,
            'cod_emp' => $in_codemp,
        ]);

        return ReBodProdDimercPDO::returnStockFormat($resultado, $first_row);
    }

    public static function getCorporateStock($in_codpro, $in_rutcliente)
    {
        $sql = "SELECT CASE WHEN (stocks-stkcom) > 0 THEN (stocks-stkcom) ELSE 0 END AS stock FROM re_bodprod_dimerc"
            . " WHERE codpro = :cod_pro AND codbod = GETRUTCONCESION_NEW(3, :rut_cliente, 0)";

        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_pro' => $in_codpro,
            'rut_cliente' => $in_rutcliente,
        ]);

        return ($resultado && count($resultado) > 0) ? $resultado[0]->stock : 0;
    }

    public static function getAllStockFulfillment() {
        $sql = "SELECT b.codpro, b.codsap, c.sap_almacen, c.sap_centro, a.stocks FROM "
            . " re_bodprod_dimerc a"
            . " INNER JOIN ma_product b ON a.codpro = b.codpro"
            . " INNER JOIN ma_bodegas c ON a.codbod = c.codbod AND c.codemp = a.codemp"
            . " WHERE a.codbod = :cod_bod_fullfilment"
            . " AND b.codneg = :cod_neg_fulfillment"
            . " AND b.codsap IS NOT NULL"
            . " AND c.sap_almacen IS NOT NULL"
            . " AND c.sap_centro IS NOT NULL"
            // . " ORDER BY codsap asc"
        ;

        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_bod_fullfilment' => 114,
            'cod_neg_fulfillment' => 1,
        ]);

        return $resultado;
    }

    public static function getAllPriceFulfillment() {
        $sql = "SELECT a.codpro, a.codsap, b.precio FROM "
            . " ma_product a"
            . " INNER JOIN re_canprod b ON a.codpro = b.codpro"
            . " WHERE a.codneg = :cod_neg_fulfillment"
            . " AND b.codcnl = :pricelist_fulfillment"
            . " AND codsap IS NOT NULL";

        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_neg_fulfillment' => 1,
            'pricelist_fulfillment' => 710
        ]);

        return $resultado;
    }

    public static function refreshStockProduct(array $products)
    {
        try {
            // DB::connection('oracle_dmventas')->beginTransaction();
            foreach ($products as $product) {
                if ($product['dimerc_codpro'] != null && trim($product['dimerc_codpro']) <> "") {
                    $jaja = $product;
                    // Formula 3
                    $sql = "MERGE INTO re_bodprod_dimerc a"
                        . " USING(SELECT :cod_emp AS codemp, :cod_bod AS codbod, :cod_pro AS codpro, "
                        . " :stock_fisico AS stocks, :stock_compr AS stkcom, :stock_disp AS stkdis FROM dual) src"
                        . " ON(a.codemp = src.codemp AND a.codbod = src.codbod AND a.codpro = src.codpro)"
                        . " WHEN MATCHED THEN"
                        . " UPDATE SET a.stocks = src.stocks, a.stkcom = src.stkcom, a.stkdis = src.stkdis"
                        . " WHEN NOT MATCHED THEN"
                        . " INSERT(codemp, codbod, codpro, stocks, stkcom, stkdis)"
                        . " VALUES(src.codemp, src.codbod, src.codpro, src.stocks, src.stkcom, src.stkdis)";

                    DB::connection('peru_oracle_dmventas')->statement($sql, [
                        'stock_fisico' => $product['sap_stkfis'],
                        'stock_compr' => $product['sap_stkcom'],
                        'stock_disp' => $product['sap_stkdis'],
                        'cod_pro' => $product['dimerc_codpro'],
                        'cod_bod' => $product['dimerc_codbod'],
                        'cod_emp' => 3,
                    ]);

                    $sql = "UPDATE sap_fbo_tf_stock SET estado = 1, FECHA_PROCE = SYSDATE";
                    DB::connection('peru_oracle_dmventas')->statement($sql);
                }
            }

            // DB::connection('oracle_dmventas')->commit();
            return true;
        } catch (\Exception $e) {
            // DB::connection('oracle_dmventas')->rollBack();
            return false;
        }
    }

    private static function returnStockFormat($registros, $first_row = false) {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $bodega = new ReBodProdDimerc();
            $bodega->setCodemp($registro->codemp);
            $bodega->setCodbod($registro->codbod);
            $bodega->setCodpro($registro->codpro);
            $bodega->setStocks($registro->stocks);
            $bodega->setStkini($registro->stkini);
            $bodega->setFecini($registro->fecini);
            $bodega->setUltinv($registro->ultinv);
            $bodega->setFecinv($registro->fecinv);
            $bodega->setStkmax($registro->stkmax);
            $bodega->setStkmin($registro->stkmin);
            $bodega->setStkcom($registro->stkcom);
            $bodega->setStkdis($registro->stkdis);
            $bodega->setStkase($registro->stkase);
            $bodega->setStkcri($registro->stkcri);
            $bodega->setStkint($registro->stkint);
            $bodega->setUsrCreac($registro->usr_creac);
            $bodega->setFechaCreac($registro->fecha_creac);
            $bodega->setUsrModif($registro->usr_modif);
            $bodega->setFechaModif($registro->fecha_modif);
            $bodega->setCosto($registro->costo);
            $bodega->setCosprom($registro->cosprom);
            $bodega->setCosproCalKardex($registro->cospro_cal_kardex);
            $bodega->setValorCalKardex($registro->valor_cal_kardex);
            $bodega->setStockCalKardex($registro->stock_cal_kardex);
            if($first_row) {
                return $bodega;
            }

            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }
}