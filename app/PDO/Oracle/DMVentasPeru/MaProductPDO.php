<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 11:03
 */

namespace App\PDO\Oracle\DMVentasPeru;

use App\Entities\Oracle\DMVentas\MaProduct;
use \DB;
use Illuminate\Database\Eloquent\Model;

class MaProductPDO extends Model
{
    public static function productosExisten($str_prods)
    {
        $regex = "/^(?:[\'][aA-zZ]{1,2}\d{5,6}[\']+,)*[\'][aA-zZ]{1,2}\d{5,6}[\']+$/";

        preg_match($regex, trim($str_prods), $match);

        // if match array is empty (no match), return false
        if (empty($match)) {
            return false;
        }

        $sql = "SELECT COUNT(*) AS existe"
            . " FROM ma_product"
            . " WHERE codpro in ($match[0])";

        $resultado = DB::connection('peru_oracle_dmventas')->select($sql);

        // if the resultant count isn't equal to the substrings quantity return false
        return ($resultado != null && $resultado[0]->existe == count(explode(",", $str_prods)))
            ? true : false;
    }

    public static function existsProductByCod($in_codpro)
    {
        $sql = "SELECT COUNT(*) AS existe FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function sapGetMaterialProdByCod($in_codpro)
    {
        $sql = "SELECT codsap FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) > 0) ? $resultado[0]->codsap : null;
    }

    public static function getProductByCod($in_codpro, $first_row = true) {
        $sql = "SELECT * FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('peru_oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return MaProductPDO::returnProductFormat($resultado, true);
    }

    public static function getAllSapProductsPeru() {
        $sql = "SELECT codpro, codsap FROM ma_product WHERE codsap IS NOT NULL";
        $resultado = DB::connection('peru_oracle_dmventas')->select($sql);
        return MaProductPDO::returnProductFormat($resultado);
    }

    private static function returnProductFormat($registros, $first_row = false) {
        $arrayReturn = null;
        foreach($registros as $registro) {
            $bodega = new \App\Entities\Oracle\DMVentasPeru\MaProduct($registro);
            if($first_row) {
                return $bodega;
            }
            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }
}
