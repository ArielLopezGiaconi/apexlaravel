<?php   

namespace App\Helpers\Consultas;

use Illuminate\Http\Request;
use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\PreciosController;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;


class PreciosHelper 
{
    public function consultaPrecio(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $rut = $datos->get("Rut", null);
        $codpro = $datos->get("Codpro", null);
        $mensaje = "";

        if(empty($rut) || empty($codpro)){
            $mensaje = "Debe Ingresar un rut y un producto.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaCliente($rut);

        if($mensaje == "0"){
            $mensaje = "Cliente no se encuentra en la base de datos.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaProducto($codpro);

        if($mensaje == "0"){
            $mensaje = "Producto no se encuentra en la base de datos.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $controlador = new PreciosController();
        return $controlador->consultaPrecio($codpro, $rut);
    }

    public function ValidaCliente(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $rut = $datos->get("Rut", null);
        $mensaje = ProductoServicePDO::ValidaCliente($rut);

        $responseFormatt->setCode(301)
            ->setResponse($mensaje);

        return response()->json($responseFormatt->getResponseFormatt());

    }

    public function ValidaProducto(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $codpro = $datos->get("Codpro", null);
        $mensaje = ProductoServicePDO::ValidaProducto($codpro);

        
        $responseFormatt
        ->setCode(301)
        ->setResponse($mensaje);

        return response()->json($responseFormatt->getResponseFormatt());

    }

}