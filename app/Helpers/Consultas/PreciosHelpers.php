<?php   

namespace App\Helpers\Consultas;

use Illuminate\Http\Request;
use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\PreciosController;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;


class PreciosHelpers 
{
    public function consultaPrecio(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $rut = $datos->get("rut", null);
        $codpro = $datos->get("Codpro", null);
        $mensaje = "";

        if(empty($rut) || empty($codpro)){
            $mensaje = "Debe Ingresar un rut y un producto.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaCliente($rut);

        if($mensaje == "0"){
            $mensaje = "Cliente no se encuentra en la base de datos.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaProducto($codpro);

        if($mensaje == "0"){
            $mensaje = "Producto no se encuentra en la base de datos.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $controlador = new PreciosController();
        return $controlador->consultaPrecio($codpro, $rut);
    }

    public function ValidaCliente(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $rut = $datos->get("rut", null);
        $mensaje = ProductoServicePDO::ValidaCliente($rut);

        $responseFormatt->setCode(200)
            ->setResponse($mensaje);

        return response()->json($responseFormatt->getResponseFormatt());

    }

    public function ValidaProducto(Request $datos)
    {
        $responseFormatt = new ResponseFormatt();
        $codpro = $datos->get("Codpro", null);
        $mensaje = ProductoServicePDO::ValidaProducto($codpro);

        $responseFormatt
        ->setCode(200)
        ->setResponse($mensaje);

        return response()->json($responseFormatt->getResponseFormatt());

    }

    public function validaProductos(Request $request)
    {
        $responseFormatt = new ResponseFormatt();
        $rut = $request->get('rut',null);
        $coddir = $request->get('coddir', null);
        $productos = $request->get("productos",null);
        //dd($productos);

        if(trim($rut)  == "" || trim($coddir) == ""){
            $mensaje = "Debe Ingresar un rut y un coddir.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaCliente($rut);

        if($mensaje == "0"){
            $mensaje = "Cliente no se encuentra en la base de datos.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $mensaje = ProductoServicePDO::ValidaCoddir($rut, $coddir);

        if($mensaje == "0"){
            $mensaje = "Codigo de dirección no asociada al cliente.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $productos = $request->get('productos', null);

        if (!isset($productos)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró el arreglo [productos]");
            return $respuesta->returnToJson();
        }

        if (!is_array($productos) || count($productos) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("El arreglo [productos] esta vacio");
            return $respuesta->returnToJson();
        }

        $i = 0;
        foreach ($productos as $producto) {
            if (!array_key_exists('sku', $producto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [codpro]");
                return $respuesta->returnToJson();
            }

            $i++;
        }

        $controlador = new PreciosController();
        return $controlador->validaPrecios($rut, $productos, $coddir);

    }

}