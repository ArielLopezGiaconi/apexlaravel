<?php   

namespace App\Helpers\Consultas;

use Illuminate\Http\Request;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;
use App\Http\Controllers\costoController;


class costoHelper 
{
    public function ConsultaCosto(Request $request)
    {
        $responseFormatt = new ResponseFormatt();
        
        $empresa = $request->get('empresa',null);
        $peso = $request->get('peso',null);
        $coddir = $request->get('coddir', null);

        if(trim($empresa)  == ""){
            $mensaje = "Debe Ingresar una empresa.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        if(trim($peso)  == ""){
            $mensaje = "Debe Ingresar un peso.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        if(trim($coddir) == ""){
            $mensaje = "Debe Ingresar un coddir.";
            
            $responseFormatt
                ->setCode(400)
                ->setResponse($mensaje);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $controlador = new costoController();
        $respuesta = $controlador->consultaCosto($empresa, $coddir, $peso);
        $responseFormatt
                ->setCode(200)
                ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
    }

    public function prorrateo(Request $request)
    {
        try {
            $formato = new ResponseFormatt();
            
            $empresa = $request->get('empresa',null);
            $pesos = $request->get('pesos',null);
            $coddir = $request->get('coddir', null);

            $i = 0;
            $total = 0;
            
            foreach ($pesos as $peso) {
                    
                if (trim($peso['kilos']) == "") {
                    $formato->setCode(201)->setResponse("El arreglo [pesos][" . $i . "] no contiene el atributo [kilos]");
                    return $formato->returnToJson();
                }
                
                $total = ($total + $peso['kilos']);
                $i++;
            }
            
            $controlador = new costoController();
            $Respuesta = $controlador->consultaProrrateo($empresa, $coddir, $pesos, $total);

            $Respuesta = $controlador->convierteArrayXML($Respuesta);
            //dd($Respuesta);

            return $Respuesta;

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

}