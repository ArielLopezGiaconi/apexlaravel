<?php   

namespace App\Helpers\Apex;

use Illuminate\Http\Request;
use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\ApexController;


class ApexHelper 
{
    public function ApexV1(Request $request)
    {
        $responseFormatt = new ResponseFormatt();
        $mensaje = $request;

        $controlador = new ApexController();
        $datos = $request->get('MT_MM020_REQ',null);

        $mensaje = $controlador->ApexExecV1($datos);
        
        return $mensaje;
        // $datos = $request->get('MT_MM020_REQ',null);
        // return $datos["Producto"]["Medidas"];

    }
}