<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;
use App\PDO\Lib\ResponseFormatt;
use DOMDocument;

class costoController extends Controller{

    public function consultaProrrateo($empresa, $coddir, $pesos, $total)
    {
        try {
            $returnCostos = array();
            $responseFormatt = new ResponseFormatt();
            $controlador = new costoController();
            
            if($empresa == 3){
                $respuesta = $controlador->consultaCostoLitcargo($coddir, $total);
            }  
            
            //dd($respuesta);
            $costo = $respuesta['costo'];

            foreach ($pesos as $kilos) {
                $kilos = $kilos['kilos'];
                $i = 0;
                
                $returnCostos[] = array(
                    "kilos"=>$kilos,
                    "precio"=>round(($kilos * $costo) / $total,0),
                );

                $i++;
            }

            // $responseFormatt = new ResponseFormatt();

            // $responseFormatt->setCode(200)
            //     ->setResponse($returnCostos);

            // return $responseFormatt->returnToJson();
            return $returnCostos;

        } catch (\Exception $e) {
            // $responseFormatt = new ResponseFormatt();

            // $responseFormatt->setCode(401)
            //     ->setResponse($returnCostos);

            // return $responseFormatt->returnToJson();
            return $e->getMessage();
        }
    }

    public function consultaCosto($empresa, $coddir, $peso) 
    {
        $responseFormatt = new ResponseFormatt();
        $controlador = new costoController();

        if($empresa == 1){
            $mensaje = $controlador->consultaCostoLitcargo($coddir, $peso);
            
        }
        
        return $mensaje;

    }

    public function consultaCostoLitcargo($coddir, $peso) 
    {
        
        $responseFormatt = new ResponseFormatt();
        $costoHUB = 98;
        $costoRM = 59;

        $mensaje = ProductoServicePDO::ValidaDireccion($coddir);

        if($mensaje == "0"){
            $mensaje = "El codigo de Direccion no existe. (". $coddir . ")";

            return $mensaje;
        }

        $condicion = $mensaje[0]->condicion; 
        $cobro = $mensaje[0]->cobro;
        $destino = $mensaje[0]->destino;

        //----------------------------------------------------COBRO SANTIAGO ------------------------------------------------------
        if($cobro == 'SANTIAGO'){
            if($condicion == 'HUB'){
                $mensaje = $peso * $costoHUB;
                $respuesta = array(
                    "costo"=>$mensaje,
                    "destino"=>$destino,
                    "tarifa"=>"SANTIAGO(HUB)");
            }else{
                $mensaje = $peso * $costoRM;
                $respuesta = array(
                    "costo"=>$mensaje,
                    "destino"=>$destino,
                    "tarifa"=>"SANTIAGO");
            }
            //----------------------------------------------------COBRO REGIONES ------------------------------------------------------
        }else{
            $mensaje = ProductoServicePDO::ObtieneCostoTrans($peso, $coddir);
            $respuesta = array(
                "costo"=>$mensaje,
                "destino"=>$destino,
                "tarifa"=>"REGIONES");
        }

        return $respuesta;
    }

    public function convierteArrayXML($array) 
    {
        $xml = new DomDocument('1.0', 'UTF-8');
        $i = 0;

        $raiz = $xml->createElement('raiz');
        $raiz = $xml->appendChild($raiz);

        foreach ($array as $valor) {
            $kilos = $valor['kilos'];
            $precio = $valor['precio'];
            
            $nodo = $xml->createElement('valor');
            $nodo = $raiz->appendChild($nodo);

            $subnodo = $xml->createElement('kilos', $kilos);
            $subnodo = $nodo->appendChild($subnodo);

            $subnodo = $xml->createElement('precio', $precio);
            $subnodo = $nodo->appendChild($subnodo);
        }

        $xml->formatOutput = true;

        $el_xml = $xml->saveXML();

        return $el_xml;
    }
}