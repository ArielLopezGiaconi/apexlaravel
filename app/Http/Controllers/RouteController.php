<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;
use App\PDO\Lib\ResponseFormatt;

class RouteController extends Controller
{
    public function consultaPrecio($codpro, $rutcli) 
    {
        $responseFormatt = new ResponseFormatt();
        $respuesta = ProductoServicePDO::ValidaConvenio($codpro, $rutcli);

        $responseFormatt->setCode(200)
            ->setResponse($respuesta);

        return response()->json($responseFormatt->getResponseFormatt());
    }

    public function prueba()
    {
        return view('welcome');
    }

}
