<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PDO\Oracle\DMVentas\ProductoServicePDO;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\Entidades\ProductoEntity;
use App\PDO\Oracle\DMVentas\ProductoEntity as DMVentasProductoEntity;

class PreciosController extends Controller
{
    public function consultaPrecio($codpro, $rutcli) 
    {
        $responseFormatt = new ResponseFormatt();
        $respuesta = ProductoServicePDO::ValidaConvenio($codpro, $rutcli);

        if($respuesta <> "0")
        {
            $responseFormatt->setCode(200)
            ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
        }   

        $respuesta = ProductoServicePDO::ValidaConvenioMarco($codpro, $rutcli);

        if($respuesta <> "0")
        {
            $responseFormatt->setCode(200)
            ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $respuesta = ProductoServicePDO::ValidaCotizacion($codpro, $rutcli);

        if($respuesta <> "0")
        {
            $responseFormatt->setCode(200)
            ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $respuesta = ProductoServicePDO::ValidaCostoEspecial($codpro, $rutcli);

        if($respuesta <> "0")
        {
            $responseFormatt->setCode(200)
            ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
        }

        $respuesta = ProductoServicePDO::ValidaPrecioLista($codpro, $rutcli);

        if($respuesta <> "0")
        {
            $responseFormatt->setCode(200)
            ->setResponse($respuesta);

            return response()->json($responseFormatt->getResponseFormatt());
        }
    }

    public function prueba()
    {
        return view('welcome');
    }

    public function validaPrecios($rutcli, $productos, $coddir)
    {
        $returnProductos = array();
        
        foreach ($productos as $producto) {
            $codpro = $producto['sku'];
            $cantid = $producto['cantidad'];
            $i = 0;

            $responseFormatt = new ResponseFormatt();

            $respuesta = ProductoServicePDO::ValidaTablasProducto($codpro);

            if($respuesta <> "OK"){
                $returnProductos[] = array(
                    "codpro"=>$codpro,
                    "precio"=>"0",
                    "precio_min"=>"0",
                    "origen"=>$respuesta,
                    "costo_trans"=>"0",
                );
                $i++;
                continue;
            }

            $respuesta = ProductoServicePDO::ValidaConvenio($codpro, $rutcli);

            $costo_trans = ProductoServicePDO::ObtieneCostoTrans($codpro, $cantid, $coddir, $rutcli);

            if($respuesta <> "0")
            {
                $returnProductos[] = array(
                    "codpro"=>$respuesta[$i]->codpro,
                    "precio"=>$respuesta[$i]->precio,
                    "precio_min"=>$respuesta[$i]->precio_min,
                    "origen"=>$respuesta[$i]->origen,
                    "costo_trans"=>$costo_trans,
                );
                $i++;
                continue;
            }   

            $respuesta = ProductoServicePDO::ValidaConvenioMarco($codpro, $rutcli);

            if($respuesta <> "0")
            {
                $returnProductos[] = array(
                    "codpro"=>$respuesta[$i]->codpro,
                    "precio"=>$respuesta[$i]->precio,
                    "precio_min"=>$respuesta[$i]->precio_min,
                    "origen"=>$respuesta[$i]->origen,
                    "costo_trans"=>$costo_trans,
                );
                $i++;
                continue;
            }

            $respuesta = ProductoServicePDO::ValidaCotizacion($codpro, $rutcli);

            if($respuesta <> "0")
            {
                $returnProductos[] = array(
                    "codpro"=>$respuesta[$i]->codpro,
                    "precio"=>$respuesta[$i]->precio,
                    "precio_min"=>$respuesta[$i]->precio_min,
                    "origen"=>$respuesta[$i]->origen,
                    "costo_trans"=>$costo_trans,
                );
                $i++;
                continue;
            }

            $respuesta = ProductoServicePDO::ValidaCostoEspecial($codpro, $rutcli);

            if($respuesta <> "0")
            {
                $returnProductos[] = array(
                    "codpro"=>$respuesta[$i]->codpro,
                    "precio"=>$respuesta[$i]->precio,
                    "precio_min"=>$respuesta[$i]->precio_min,
                    "origen"=>$respuesta[$i]->origen,
                    "costo_trans"=>$costo_trans,
                );
                $i++;
                continue;
            }

            $respuesta = ProductoServicePDO::ValidaPrecioLista($codpro, $rutcli);

            if($respuesta <> "0")
            {
                $returnProductos[] = array(
                    "codpro"=>$respuesta[$i]->codpro,
                    "precio"=>$respuesta[$i]->precio,
                    "precio_min"=>$respuesta[$i]->precio_min,
                    "origen"=>$respuesta[$i]->origen,
                    "costo_trans"=>$costo_trans,
                );
                $i++;
                continue;
            }

            $i++;
        }

        $responseFormatt = new ResponseFormatt();

        $responseFormatt->setCode(200)
            ->setResponse($returnProductos);

        return $responseFormatt->returnToJson();

        try {

        } catch (\Exception $e) {
            $responseFormatt = new ResponseFormatt();

            $responseFormatt->setCode(401)
                ->setResponse($returnProductos);

            return $responseFormatt->returnToJson();
        }
    }

    
}
