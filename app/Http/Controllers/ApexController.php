<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PDO\Oracle\DMVentas\ApexPDO;
use App\PDO\Lib\ResponseFormatt;
use DOMDocument;
use App\Clases\productoModel;

class ApexController extends Controller{

    public function ApexExecV1($datos)
    {
        try {
            $responseFormatt = new ResponseFormatt();

            $producto = new productoModel();

            //datos material
            $producto->codMaterial = trim($datos["Producto"]["cod_material"],"0");
            $producto->codAnterior = $datos["Producto"]["cod_anterior"];  //codigo dimerc en caso de que sea antiguo.
            $producto->tipoProd = $datos["Producto"]["tipo_prod"];
            $producto->desCorta = $datos["Producto"]["des_corta"];
            $producto->desLarga = $datos["Producto"]["des_larga"];
            $producto->codJerarquia = $datos["Producto"]["cod_jerarquia"];
            $producto->codLinea = $datos["Producto"]["cod_linea"];
            $producto->deslinea = $datos["Producto"]["des_linea"];
            $producto->codSeccion = $datos["Producto"]["cod_seccion"];
            $producto->desSeccion = $datos["Producto"]["des_seccion"];
            $producto->codRubro = $datos["Producto"]["cod_rubro"];
            $producto->desRubro = $datos["Producto"]["des_rubro"];
            $producto->codSubRubro = $datos["Producto"]["cod_sub_rubro"];
            $producto->desSubRubro = $datos["Producto"]["des_sub_rubro"];
            $producto->codGrupo = $datos["Producto"]["cod_grupo"];
            $producto->desGrupo = $datos["Producto"]["des_grupo"];
            $producto->codMarca = $datos["Producto"]["cod_marca"];
            $producto->desMarca = $datos["Producto"]["des_marca"];
            $producto->pesoBruto = $datos["Producto"]["peso_bruto"];
            $producto->pesoNeto = $datos["Producto"]["peso_neto"];
            $producto->uniPeso = $datos["Producto"]["uni_peso"];
            $producto->volumen = $datos["Producto"]["volumen"];
            $producto->uniVolumen = $datos["Producto"]["uni_volumen"];
            $producto->uniBase = $datos["Producto"]["uni_base"];
            $producto->uniEmbalaje = $datos["Producto"]["uni_embalaje"];
            $producto->denConvEmb = $datos["Producto"]["den_conv_emb"];
            $producto->numConvEmb = $datos["Producto"]["num_conv_emb"];
            $producto->pesoNetoEmbalaje = $datos["Producto"]["peso_neto_embalaje"];
            $producto->pesoBrutoEmbalaje = $datos["Producto"]["peso_bruto_embalaje"];
            $producto->uniSubEmbalaje = $datos["Producto"]["uni_sub_embalaje"];
            $producto->denConvSubEmb = $datos["Producto"]["den_conv_sub_emb"];
            $producto->numConvSubEmb = $datos["Producto"]["num_conv_sub_emb"];
            $producto->pesoNetoSubEmb = $datos["Producto"]["peso_neto_sub_emb"];
            $producto->pesoBrutoSubEmb = $datos["Producto"]["peso_bruto_sub_emb"];
            $producto->UMB = $datos["Producto"]["UMB"]["cod_barra_umb"];
            $producto->embalaje = $datos["Producto"]["Embalaje"]["cod_barra_embalaje"];
            $producto->subEmbalaje = $datos["Producto"]["Subembalaje"]["cod_barra_sub_emb"];
            $producto->VolumenEmb = $datos["Producto"]["Medidas"]["volumen_emb"];
            $producto->uniPesoEmb = $datos["Producto"]["Medidas"]["uni_peso_emb"];
            $producto->uniVolumenEmb = $datos["Producto"]["Medidas"]["uni_volumen_emb"];
            $producto->volumenSubEmb = $datos["Producto"]["Medidas"]["volumen_sub_emb"];
            $producto->uniPesoSubEmb = $datos["Producto"]["Medidas"]["uni_peso_sub_emb"];
            $producto->uniVolumenSubEmb = $datos["Producto"]["Medidas"]["uni_volumen_sub_emb"];

            //cargo sociedades
            foreach ($datos["Producto"]["Sociedad"] as $sociedad)
            {
                if(isset($sociedad["afecto"]))
                {
                    $afecto = $sociedad["afecto"];
                }
                else
                {
                    $afecto = null;
                }

                if (array_key_exists('costo_medio_variable', $sociedad)){
                    $costo = $sociedad["costo_medio_variable"];
                }else{
                    $costo = 0;
                }
                
                $producto->addSociedad($sociedad["cod_material"], 
                                       $sociedad["org_ventas"], 
                                       $sociedad["canal_dist"], 
                                       $sociedad["uni_venta"],
                                       $costo, 
                                       $sociedad["costo_comercial"], 
                                       $sociedad["des_peru"], 
                                       $sociedad["a_pedido"],
                                       $sociedad["grupo_material"], 
                                       $sociedad["marca_propia"], 
                                       $sociedad["importado"], 
                                       $afecto, 
                                       $sociedad["cat_comision"], 
                                       $sociedad["cod_neg"]);
            }

            //cargo centros
            if (array_key_exists('cod_centro', $datos["Producto"]["Centros"])){
                $producto->addCentro($datos["Producto"]["Centros"]["cod_centro"], $datos["Producto"]["Centros"]["rotacion"]);
            }else{
                foreach ($datos["Producto"]["Centros"] as $centro)
                {
                    $producto->addCentro($centro["cod_centro"], $centro["rotacion"]);   
                }
            }
            
            //impuestos
            foreach($datos["Producto"]["Impuesto"] as $impuesto)
            {
                $producto->addImpuesto($impuesto["pais"], $impuesto["imp_especifico"], $impuesto["clasif_fiscal"]);
            }

            //mando producto para trabajo en Dimerc
            $respuesta = ApexPDO::Dimerc($producto);

            $dimerc = $respuesta;

            //mando producto para trabajo en Dimerc
            $respuesta = ApexPDO::DimercPeru($producto);

            $peru = $respuesta;

            //mando producto para trabajo en Dimerc
            $respuesta = ApexPDO::Unificado($producto);
            
            $unificado = $respuesta;

            if ($dimerc == 'ok') {
                $dimerc = 'Se actualizo correctamente en Dimerc Chile';
            }

            if ($peru == 'ok') {
                $peru = 'Se actualizo correctamente en Dimerc Peru';
            }

            if ($unificado == 'ok') {
                $unificado = 'Se actualizo correctamente en Unificado';
            }

            $respuesta = $dimerc . "/" . $peru . "/" . $unificado;
            
            return $respuesta;

        } catch (\Exception $e) {
            // $responseFormatt = new ResponseFormatt();

            // $responseFormatt->setCode(401)
            //     ->setResponse($returnCostos);

            // return $responseFormatt->returnToJson();
            return $e->getMessage();
        }
    }
}