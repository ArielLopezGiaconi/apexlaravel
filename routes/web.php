<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('costoTransporteEP', '\App\Helpers\Consultas\costoHelper@prorrateo');
// dd('a');
Route::post('Producto/Precio', '\App\Helpers\Consultas\PreciosHelper@consultaPrecio');
Route::post('costoTransporteM', '\App\Helpers\Consultas\costoHelper@ConsultaCosto');
Route::post('Apex/bajadaV1', '\App\Helpers\Apex\ApexHelper@ApexV1');
// Route::post('Producto/validaCliente', '\App\Helpers\Consultas\PreciosHelper@ValidaCliente');
// Route::post('Producto/validaProducto', '\App\Helpers\Consultas\PreciosHelper@ValidaProducto');
// Route::get('aa', 'RouteController@prueba');
